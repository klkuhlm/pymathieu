import mathieu_functions as mf
import numpy as np
import matplotlib.pyplot as plt

ny = 30
ymax = 60.0
ncut = 4

x = np.linspace(-ymax,ymax,2*ny)
y = np.linspace(0.0,ymax,ny)
cutoff = [1.0E-8, 1.0E-10, 1.0E-12, 1.0E-14]

buff = np.empty((ny,2*ny,ncut))

for k,c in enumerate(cutoff):
    for i,xv in enumerate(x):
        for j,yv in enumerate(y):
            m = mf.mathieu(xv + yv*j, M=75, cutoff=c)
            buff[j,i,k] = m.buffer

plt.figure()
for i in range(4):
    plt.subplot(2,2,i+1)
    plt.contourf(x,y,buff[:,:,i],40)
    cb = plt.colorbar()
    cb.set_label('buffer')
    plt.xlabel('$\\Re(q)$')
    plt.ylabel('$\\Im(q)$')
    plt.title('%i %e' %(i,cutoff[i]))

plt.savefig('buffer.png')
