import numpy as np
import mathieu_functions as mf
import matplotlib.pyplot as plt
from os import system

# compare results for real q against alhargan's program

max_ord = 7
c = ['red','green','blue','cyan','magenta','black','orange','pink','purple']
plotDiff = True
alt = '--'
klt = ':'

def alhargan(typ='e',M=7,r='c',kind=1,q=2.0):
    """write input for a driver of Alhargan's alg804,
    run code and retrun results.

    typ = (e)ven or (o)dd
    M = maximum order returned (even functions start at 0, odd at 1)
    r = (r)adial or (c)ircumferential
    kind = first (1) or second (2)
    q = Mathieu parameter (q=h^2/4 : Alhargan's code uses h)"""

    f = open('in','w')
    f.write(f'{typ}\n{M}\n{r}\n{kind}\n{2.0*np.sqrt(q):.14e}\n')
    f.close()
    
    fail = system('./alhargan_804/mathieu_804 <in >out')
    if fail:
        print('error returned from algorithm 804')

    return np.loadtxt('out',skiprows=2)

prefix = ''
anglabels = ('$0$','$\\pi/2$','$\\pi$','$3\\pi/2$','$2\\pi$')

for q in [0.05,2.0,6.0,10.0]:
    m = mf.mathieu(q,norm=2)
       
    ########### ce    
    al = alhargan(typ='e',M=max_ord,r='c',kind=1,q=q)
    psi = al[:,0]
    al = al[:,1:]

    plt.figure(1,figsize=(14,12))
    plt.subplot(211)
    mine = m.ce(np.arange(max_ord+1),psi).real
    for n in range(max_ord+1):
        if plotDiff:
            plt.plot(psi,al[:,n]-mine[n,:],'-',color=c[n],label='%i'%n)
        else:
            plt.plot(psi,al[:,n],alt,color=c[n],label='%i'%n)
            plt.plot(psi,mine[n,:],klt,color=c[n])
    plt.xlim([0,2*np.pi])
    plt.xlabel('$\\psi$')
    plt.xticks(np.linspace(0,2*np.pi,5),anglabels)
    plt.ylabel('Alhargan 804 lib - my Py lib')
    plt.legend(loc=0,ncol=max_ord)
    plt.title('ce q=%.2f'%q)
    
    ########### se  
    al = alhargan(typ='o',M=max_ord,r='c',kind=1,q=q)
    al = al[:,1:]
    
    plt.subplot(212)
    mine = m.se(np.arange(max_ord+2),psi).real
    for n in range(max_ord):
        if plotDiff:
            plt.plot(psi,al[:,n]-mine[n+1,:],'-',color=c[n+1],label='%i'%n)
        else:
            plt.plot(psi,al[:,n],alt,color=c[n+1],label='%i'%(n+1))
            plt.plot(psi,mine[n+1,:],klt,color=c[n+1])
    plt.xlim([0,2*np.pi])
    plt.xlabel('$\\psi$')
    plt.xticks(np.linspace(0,2*np.pi,5),anglabels)
    plt.legend(loc=0,ncol=max_ord)
    plt.title('se  q=%.2f'%q)
    plt.savefig('%salhargan_compare_%.2f_ang.png'%(prefix,q))
    plt.close(1)

    ########### Je    
    al = alhargan(typ='e',M=max_ord,r='r',kind=1,q=q)
    eta = al[:,0]
    seta = np.sinh(eta)
    al = al[:,1:]
    
    plt.figure(1,figsize=(15,15))
    plt.subplot(221)
    mine = m.Je(np.arange(max_ord+1),eta).real
    for n in range(max_ord+1):
        if plotDiff:
            plt.plot(seta,al[:,n]-mine[n,:],'-',color=c[n],label='%i'%n)
        else:
            plt.plot(seta,al[:,n],alt,color=c[n],label='%i'%n)
            plt.plot(seta,mine[n,:],klt,color=c[n])
    plt.plot([seta[0],seta[-1]],[0.0,0.0],'k-',linewidth=0.25)
    plt.xlim((seta[0],seta[-1]))
    plt.ylabel('Alhargan 804 lib / my Py lib')
    plt.legend(loc=0,ncol=max_ord//2)
    plt.title('Je q=%.2f'%q)
    #np.savetxt(prefix+'Je_alhargan_compare.txt',
    #           np.concatenate((eta[:,None],al[:,:],al[:,:]-mine.T),axis=1))
    
    ########### Jo    
    al = alhargan(typ='o',M=max_ord,r='r',kind=1,q=q)
    al = al[:,1:]
    
    plt.subplot(222)
    mine = m.Jo(np.arange(1,max_ord+2),eta).real
    for n in range(max_ord):
        if plotDiff:
            plt.plot(seta,al[:,n]-mine[n+1,:],'-',color=c[n+1],label='%i'%n)
        else:
            plt.plot(seta,al[:,n],alt,color=c[n+1],label='%i'%(n+1))
            plt.plot(seta,mine[n+1,:],klt,color=c[n+1])
    plt.plot([seta[0],seta[-1]],[0.0,0.0],'k-',linewidth=0.25)
    plt.xlim((seta[0],seta[-1]))
    plt.legend(loc=0,ncol=max_ord//2)
    plt.title('Jo q=%.2f'%q)
    #np.savetxt(prefix+'Jo_alhargan_compare.txt',
    #           np.concatenate((eta[:,None],al[:,:],al[:,:]-mine.T),axis=1))
    
    ########### Ne    
    al = alhargan(typ='e',M=max_ord,r='r',kind=2,q=q)
    al = al[:,1:]
    
    plt.subplot(223)
    mine = m.Ne(np.arange(max_ord+1),eta).real
    for n in range(max_ord+1):
        if plotDiff:
            plt.plot(seta,al[:,n]-mine[n,:],'-',color=c[n],label='%i'%n)
        else:
            plt.plot(seta,al[:,n],alt,color=c[n],label='%i'%n)
            plt.plot(seta,mine[n,:],klt)
    plt.plot([seta[0],seta[-1]],[0.0,0.0],'k-',linewidth=0.25)
    plt.xlim((seta[0],seta[-1]))
    plt.xlabel('$\\sinh(\\eta)$')
    plt.ylabel('Alhargan 804 lib / my Py lib')
    plt.legend(loc=0,ncol=max_ord//2)
    plt.title('Ne q=%.2f'%q)
    #np.savetxt(prefix+'Ne_alhargan_compare.txt',
    #           np.concatenate((eta[:,None],al[:,:],al[:,:]-mine.T),axis=1))
    
    ########### No    
    al = alhargan(typ='o',M=max_ord,r='r',kind=2,q=q)
    al = al[:,1:]
    
    plt.subplot(224)
    mine = m.No(np.arange(1,max_ord+2),eta).real
    for n in range(max_ord):
        if plotDiff:
            plt.plot(seta,al[:,n]-mine[n+1,:],'-',color=c[n+1],label='%i'%n)
        else:
            plt.plot(seta,al[:,n],alt,color=c[n+1],label='%i'%(n+1))
            plt.plot(seta,mine[n+1,:],klt,color=c[n+1])
    plt.plot([seta[0],seta[-1]],[0.0,0.0],'k-',linewidth=0.25)
    plt.xlim((seta[0],seta[-1]))
    plt.xlabel('$\\sinh(\\eta)$')
    plt.title('No q=%.2f'%q)
    plt.legend(loc=0,ncol=max_ord//2)
    plt.savefig('%salhargan_compare_%.2f_radial.png'%(prefix,q))
    #np.savetxt(prefix+'No_alhargan_compare.txt',
    #           np.concatenate((eta[:,None],al[:,:],al[:,:]-mine.T),axis=1))
    plt.close(1)
