import numpy as np
import mathieu_functions as mf
import mod_mathieu_functions as mmf
import matplotlib.pyplot as plt
from os import system

# compare results for real q against alhargan's program

max_ord = 4

c = ['r','g','b','c','m','k']

def alhargan(typ='e',M=5,r='c',kind=1,q=2.0):
    """write input for a driver of Alhargan's alg804,
    run code and retrun results.

    typ = (e)ven or (o)dd
    M = maximum order returned (even functions start at 0, odd at 1)
    r = (r)adial or (c)ircumferential
    kind = first (1) or second (2)
    q = Mathieu parameter (q=h^2/4 : Alhargan's code uses h)"""

    f = open('in','w')
    f.write(f'{typ}\n{M}\n{r}\n{kind}\n{2.0*np.sqrt(q):.14e}\n')
    f.close()
    
    fail = system('./alhargan_804/mathieu_804 <in >out')
    if fail:
        print('error returned from algorithm 804')

    return np.loadtxt('out',skiprows=2)

def mod_alhargan(typ='e',M=5,r='c',kind=1,q=2.0):
    """write input for a driver of Alhargan's alg804,
    run code and retrun results.

    typ = (e)ven or (o)dd
    M = maximum order returned (even functions start at 0, odd at 1)
    r = (r)adial or (c)ircumferential
    kind = first (1) or second (2)
    q = Mathieu parameter (q=h^2/4 : Alhargan's code uses h)"""

    f = open('in','w')
    f.write(f'{typ}\n{M}\n{r}\n{kind}\n{2.0*np.sqrt(q + 0.0*1j):.14e}\n')
    f.close()
    
    fail = system('./alhargan_804/mmathieu_804 <in >out')
    if fail:
        print('error returned from modified algorithm 804')

    return np.loadtxt('out',skiprows=2)

prefix = ''

anglabels = ('$0$','$\\pi/2$','$\\pi$','$3\\pi/2$','$2\\pi$')

for q in np.linspace(0.01,10.0,num=4):
    m = mf.mathieu(q,norm=2)
    mm = mmf.mathieu(-q,norm=2)
       
    ########### ce    
    al = alhargan(typ='e',M=max_ord,r='c',kind=1,q=q)
    alm = mod_alhargan(typ='e',M=max_ord,r='c',kind=1,q=-q)

    psi = al[:,0]
    al = al[:,1:]

    plt.figure(1)
    plt.subplot(221)
    for n in range(max_ord+1):
        plt.plot(psi,al[:,n]-m.ce(n,psi)) # plot difference of angular functions (many zeros)
    plt.xlim([0,2*np.pi])
    plt.xlabel('$\\psi$')
    plt.xticks(np.linspace(0,2*np.pi,5),anglabels)
    plt.ylabel('Alhargan 804 lib - my Py lib')
    plt.title('ce q=%.2f'%q)
    
    plt.subplot(223)
    for n in range(max_ord+1):
        plt.plot(psi,alm[:,n]-mm.ce(n,psi)) 
    plt.xlim([0,2*np.pi])
    plt.xlabel('$\\psi$')
    plt.xticks(np.linspace(0,2*np.pi,5),anglabels)
    plt.ylabel('Alhargan 804 lib - my Py lib')
    plt.title('ce q=-%.2f'%q)

    ########### se  
    al = alhargan(typ='o',M=max_ord,r='c',kind=1,q=q)
    alm = mod_alhargan(typ='o',M=max_ord,r='c',kind=1,q=-q)

    al = al[:,1:]
    
    plt.subplot(222)
    for n in range(max_ord):
        plt.plot(psi,al[:,n]-m.se(n+1,psi))
    plt.xlim([0,2*np.pi])
    plt.xlabel('$\\psi$')
    plt.xticks(np.linspace(0,2*np.pi,5),anglabels)
    plt.title('se  q=%.2f'%q)

    plt.subplot(224)
    for n in range(max_ord):
        plt.plot(psi,alm[:,n]-mm.se(n+1,psi))
    plt.xlim([0,2*np.pi])
    plt.xlabel('$\\psi$')
    plt.xticks(np.linspace(0,2*np.pi,5),anglabels)
    plt.title('se  q=-%.2f'%q)
    plt.savefig('%salhargan_compare_%.2f_ang.eps'%(prefix,q))
    
    plt.close(1)

    ########### Ie    
    al = mod_alhargan(typ='e',M=max_ord,r='r',kind=1,q=-q)
    eta = al[:,0]
    al = al[:,1:]
    
    plt.figure(1)
    plt.subplot(221)
    mine = mm.Ie(np.arange(max_ord+1),eta)
    # plot ratio of radial functions (huge range of size)
    for n in range(max_ord+1):
        plt.plot(np.sinh(eta),np.log10(np.abs(al[:,n]/mine[n,:])),c[n]+'-')
    plt.plot([np.sinh(eta[0]),np.sinh(eta[-1])],[0.0,0.0],'k:')
    plt.xlim((0,np.sinh(eta[-1])))
    plt.ylabel('Alhargan 804 lib / my Py lib')
    plt.title('Ie q=%.2f'%q)
    np.savetxt(prefix+'Ie_alhargan_compare.txt',
               np.concatenate((eta[:,None],al[:,:],al[:,:]-mine.T),axis=1))
    
    ########### Io    
    al = mod_alhargan(typ='o',M=max_ord,r='r',kind=1,q=-q)
    al = al[:,1:]
    
    plt.subplot(222)
    mine = mm.Io(np.arange(1,max_ord+1),eta)
    for n in range(max_ord):
        plt.plot(np.sinh(eta),np.log10(np.abs(al[:,n]/mine[n,:])),c[n]+'-')
    plt.plot([np.sinh(eta[0]),np.sinh(eta[-1])],[0.0,0.0],'k:')
#   plt.xlabel('$\\eta$')
#   plt.ylabel('Io')
    plt.xlim((0,np.sinh(eta[-1])))
    plt.title('Io q=%.2f'%q)
    np.savetxt(prefix+'Io_alhargan_compare.txt',
               np.concatenate((eta[:,None],al[:,:],al[:,:]-mine.T),axis=1))
    
    ########### Ke    
    al = mod_alhargan(typ='e',M=max_ord,r='r',kind=2,q=-q)
    al = al[:,1:]
    
    plt.subplot(223)
    mine = mm.Ke(np.arange(max_ord+1),eta)
    for n in range(max_ord+1):
        plt.plot(np.sinh(eta),np.log10(np.abs(al[:,n]/mine[n,:])),c[n]+'-')
    plt.plot([np.sinh(eta[0]),np.sinh(eta[-1])],[0.0,0.0],'k:')
    plt.xlabel('$\\sinh(\\eta)$')
    plt.xlim((0,np.sinh(eta[-1])))
    plt.ylabel('Alhargan 804 lib / my Py lib')
    plt.title('Ke q=%.2f'%q)
    np.savetxt(prefix+'Ke_alhargan_compare.txt',
               np.concatenate((eta[:,None],al[:,:],al[:,:]-mine.T),axis=1))
    
    ########### Ko    
    al = mod_alhargan(typ='o',M=max_ord,r='r',kind=2,q=-q)
    al = al[:,1:]
    
    plt.subplot(224)
    mine = mm.Ko(np.arange(1,max_ord+1),eta)
    for n in range(max_ord):
        plt.plot(np.sinh(eta),np.log10(np.abs(al[:,n]/mine[n,:])),c[n]+'-')
    plt.plot([np.sinh(eta[0]),np.sinh(eta[-1])],[0.0,0.0],'k:')
    plt.xlabel('$\\sinh(\\eta)$')
#    plt.ylabel('Ko')
    plt.xlim((0,np.sinh(eta[-1])))
    plt.title('Ko q=%.2f'%q)
    plt.savefig('%salhargan_compare_%.2f_radial.eps'%(prefix,q))
    np.savetxt(prefix+'Ko_alhargan_compare.txt',
               np.concatenate((eta[:,None],al[:,:],al[:,:]-mine.T),axis=1))
    plt.close(1)
