import numpy as np
import matplotlib.pyplot as plt
import mathieu_functions as mf

axw = 0.25
norm = 1

# ########################################
# page 31, Figure 3-3

fig = plt.figure(1,figsize=(10,10))

mv = []
for q in range(6):
    mv.append(mf.mathieu(q,norm=norm))

psi = np.linspace(0,2*np.pi,400)

# upper left
ax = fig.add_subplot(221)
for m in mv:
    ax.plot(psi,m.ce(1,psi).real)
ax.plot([0,2*np.pi],[0,0],'k-',linewidth=axw)
ax.set_xlim([0,2*np.pi])
ax.set_xlabel('$\\psi$')
ax.set_ylabel('ce$_1(\\psi,q)$')

# lower left
ax = fig.add_subplot(223)
for m in mv:
    ax.plot(psi,m.se(1,psi).real)
ax.plot([0,2*np.pi],[0,0],'k-',linewidth=axw)
ax.set_xlim([0,2*np.pi])
ax.set_xlabel('$\\psi$')
ax.set_ylabel('ce$_n(\\psi,1)$')

del mv
m = mf.mathieu(1,norm=norm)
psi = np.linspace(0,np.pi/2,200)

# upper right
ax = fig.add_subplot(222)

ord = np.arange(6)
ce = m.ce(ord,psi)

for n in ord:
    ax.plot(psi,ce[n,:].real)
ax.plot([0,np.pi/2],[0,0],'k-',linewidth=axw)
ax.set_xlim([0,np.pi/2])
ax.set_xlabel('$\\psi$')
ax.set_ylabel('se$_1(\\psi,q)$')

# lower right
ax = fig.add_subplot(224)

m = mf.mathieu(5,norm=norm)
se = m.se(ord[1:],psi)

for n in ord[:-1]:
    ax.plot(psi,se[n,:].real)
ax.plot([0,np.pi/2],[0,0],'k-',linewidth=axw)
ax.set_xlim([0,np.pi/2])
ax.set_xlabel('$\\psi$')
ax.set_ylabel('se$_n(\\psi,5)$')

plt.suptitle('page 31 Figure 3-3')
plt.subplots_adjust(wspace=0.35)
plt.savefig('gv-fig3-3.png')
plt.close(1)

# ########################################
# page 32, Figure 3-4

fig = plt.figure(1,figsize=(10,10))
eta = np.linspace(0,2.5,400)

mv = [mf.mathieu(1,norm=norm),
      mf.mathieu(5,norm=norm),
      mf.mathieu(25,norm=norm)]

# upper left
ax = fig.add_subplot(221)
ax.plot(np.sinh(eta),mv[0].Je(0,eta).real,'--')
ax.plot(np.sinh(eta),mv[2].Je(0,eta).real,'-')
ax.plot([0,6],[0,0],'k-',linewidth=axw)
ax.axis('tight')
ax.set_xlabel('sinh$(\\eta)$')
ax.set_ylabel('Je$_0(\\eta,\{1,25\})$')

# upper right
ax = fig.add_subplot(222)
ax.plot(eta,mv[1].Jo(1,eta).real,'--')
ax.plot(eta,mv[2].Jo(1,eta).real,'-')
ax.plot([0,2.5],[0,0],'k-',linewidth=axw)
ax.axis('tight')
ax.set_xlabel('$\\eta$')
ax.set_ylabel('Jo$_1(\\eta,\{5,25\})$')

# lower left
ax = fig.add_subplot(223)
ax.plot(np.sinh(eta),mv[0].Ne(0,eta).real,'--')
ax.plot(np.sinh(eta),mv[2].Ne(0,eta).real,'-')
ax.plot([0,6],[0,0],'k-',linewidth=axw)
ax.axis('tight')
ax.set_xlabel('sinh$(\\eta)$')
ax.set_ylabel('Ne$_0(\\eta,\{1,25\})$')

# lower right
ax = fig.add_subplot(224)
ax.plot(eta,mv[1].No(1,eta).real,'--')
ax.plot(eta,mv[2].No(1,eta).real,'-')
ax.plot([0,2.5],[0,0],'k-',linewidth=axw)
ax.axis('tight')
ax.set_xlabel('$\\eta$')
ax.set_ylabel('No$_1(\\eta,\{5,25\})$')

plt.suptitle('page 33 Figure 3-4')
plt.subplots_adjust(wspace=0.35)
plt.savefig('gv-fig3-4.png')
plt.close(1)

# ########################################
# page 33, Figure 3-5

fig = plt.figure(1,figsize=(10,10))
eta = np.linspace(0,2.5,400)

mv = []
for q in range(1,7):
    mv.append(mf.mathieu(q,norm=norm))

# upper left
ax = fig.add_subplot(221)
for m in mv:
   ax.plot(eta,m.Je(3,eta).real)
ax.plot([0,2],[0,0],'-')
ax.axis('tight')
ax.set_xlabel('$\\eta$')
ax.set_ylabel('Je$_3(\\eta,\{1,6\})$')

# upper right
ax = fig.add_subplot(222)
for m in mv:
    ax.plot(eta,m.Jo(3,eta).real)
ax.plot([0,2.5],[0,0],'-')
ax.axis('tight')
ax.set_xlabel('$\\eta$')
ax.set_ylabel('Jo$_3(\\eta,\{1,6\})$')

# lower left
ax = fig.add_subplot(223)
for m in mv:
    ax.plot(eta,m.Ne(1,eta).real)
ax.plot([0,2.5],[0,0],'-')
ax.axis('tight')
ax.set_xlabel('$\\eta$')
ax.set_ylabel('Ne$_1(\\eta,\{1,6\})$')

# lower right
ax = fig.add_subplot(224)
for m in mv:
    ax.plot(eta,m.No(1,eta).real)
ax.plot([0,2.5],[0,0],'-')
ax.axis('tight')
ax.set_xlabel('$\\eta$')
ax.set_ylabel('No$_1(\\eta,\{1,6\})$')

plt.suptitle('page 34 Figure 3-5')
plt.subplots_adjust(wspace=0.35)
plt.savefig('gv-fig3-5.png')
plt.close(1)

# the following figure is of Modified Mathieu functions
### ########################################
### page 34, Figure 3-6
##
##fig = plt.figure(1,figsize=(10,10))
##eta = np.linspace(0,1.0,400)
##
##mv = []
##for q in range(1,7):
##    mv.append(mf.mathieu(-q,norm=norm))
##
### upper left
##ax = fig.add_subplot(221)
##for m in mv[:-1]:
##    ax.plot(eta,m.Ie(0,eta).real)
##ax.set_xlim([0,0.7])
##ax.set_xlabel('$\\eta$')
##ax.set_ylabel('Ie$_0(\\eta,\{-1,-5\})$')
##
### upper right
##ax = fig.add_subplot(222)
##for m in mv[:-1]:
##    ax.plot(eta,m.Io(1,eta).real)
##ax.set_xlim([0,0.7])
##ax.set_xlabel('$\\eta$')
##ax.set_ylabel('Io$_1(\\eta,\{-1,-5\})$')
##
### lower left
##ax = fig.add_subplot(223)
##for m in mv:
##    ax.plot(eta,m.Ke(3,eta).real)
##ax.set_xlim([0,1])
##ax.set_xlabel('$\\eta$')
##ax.set_ylabel('Ke$_1(\\eta,\{-1,-6\})$')
##
### lower right
##ax = fig.add_subplot(224)
##for m in mv:
##    ax.plot(eta,m.Ko(1,eta).real)
##ax.set_xlim([0,1])
##ax.set_xlabel('$\\eta$')
##ax.set_ylabel('Ko$_1(\\eta,\{-1,-6\})$')
##
##plt.suptitle('page 34 Figure 3-6')
##plt.subplots_adjust(wspace=0.35)
##plt.savefig('gv-fig3-6.png')
##plt.close(1)

