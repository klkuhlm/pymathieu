import os
import numpy as np
import mathieu_functions as mf
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def plot_a_double_points(dpa,k):
    # add double points to all three subplots
    # 1a) plot first-quadrant double points
    plt.plot(dpa[k][:,0],dpa[k][:,1],'*k')
    # 1b) double points are connections between mcn differing by two orders
    if k>=2:
        plt.plot(dpa[k-2][:,0],dpa[k-2][:,1],'ok')

    # 2) plot second-quadrant double points
    # 2a) even orders have L/R symmetry
    if k%2 == 0:
        plt.plot(-dpa[k][:,0],dpa[k][:,1],'*k')
        if k>=2:
            plt.plot(-dpa[k-2][:,0],dpa[k-2][:,1],'ok')
    # 2b) odd orders are a/b swapped and symmetric
    else: 
        plt.plot(-dpb[k-1][:,0],dpb[k-1][:,1],'*k')
        if k>2:
            plt.plot(-dpb[k-1-2][:,0],dpb[k-1-2][:,1],'ok')

def plot_b_double_points(dpb,k):
    plt.plot(dpb[k][:,0],dpb[k][:,1],'*k')
    if k>=3:
        plt.plot(dpb[k-2][:,0],dpb[k-2][:,1],'ok')
    if (k+1)%2 == 0: # b begins at 1, rather than 0 like a, hence (k+1)%2
        plt.plot(-dpb[k][:,0],dpb[k][:,1],'*k')
        if k>=3:
            plt.plot(-dpb[k-2][:,0],dpb[k-2][:,1],'ok')
    else: # odd order
        plt.plot(-dpa[k+1][:,0],dpa[k+1][:,1],'*k')
        if k>=2:
            plt.plot(-dpa[k+1-2][:,0],dpa[k+1-2][:,1],'ok')

import matplotlib.font_manager as fnt
fxs = fnt.FontProperties(size='x-small')
fs = fnt.FontProperties(size='small')

surface = True
max_mcn = 8 # <--- should be even, need double points entered in below too


# #########################################
# #########################################
# plot surfaces of mcn for complex q

if surface:
    # since code implicitly assumes Re(q)<0
    # the real axis is effectively mirrored here
    re = np.linspace(-25.0,25.0,num=50)
    im = np.linspace(0.0,50.0,num=50)
    
    ax = [re[0],re[-1],im[0],im[-1]]
    
    X,Y = np.meshgrid(re,im)
    
    Z = X + Y*1j
        
    mcn_a = np.empty((len(im),len(re),max_mcn),dtype=complex)
    mcn_b = np.empty((len(im),len(re),max_mcn),dtype=complex)
    
    # same for both normalizations
    for i in range(len(re)):
        # takes a while to run, print status
        print(i)
        for j in range(len(im)):
            # rough heuristic for picking M
            M = max(16,int(1.5*abs(Z[j,i])))

            m = mf.mathieu(q=Z[j,i],M=M)
            
            for k in range(max_mcn/2):
                # for mathieu characteristic numbers
                # mcn[:,j], j=0 a_{2n}    
                #           j=1 a_{2n+1}  
                #           j=2 b_{2n+2}  
                #           j=3 b_{2n+1}  
                
                # starts at a_0 -> ce_0
                mcn_a[j,i,2*k]   = m.mcn[k,0]
                mcn_a[j,i,2*k+1] = m.mcn[k,1]
                
                # starts at b_1 -> se_1
                mcn_b[j,i,2*k+1]= m.mcn[k,2]
                mcn_b[j,i,2*k]  = m.mcn[k,3]


    # flip matrices
    mcn_a = mcn_a[::-1,:]
    mcn_b = mcn_b[::-1,:]
                    
    ###########################################
    #  index |        dp symm |        dp symm  
    # -----------------------------------------
    #   0    |   a_0    self  |   b_1   no dp (for Re(q) > 0)
    #   1    |   a_1    b_1   |   b_2   self
    #   2    |   a_2    self  |   b_3   a_3
    #   3    |   a_3    b_3   |   b_4   self


    a2r = np.pi/180.0

    # first-quadrant double points as (r,theta in degrees CCW of +x) pairs 
    # from Table 2 in (Blanch & Clemm, Math. of Comp., 1969), values for a_r 0<=r<=8
    dpa = [[[1.47,90.0],], [[3.77,59.18],], [[7.27,44.61],], [[11.98,36.02],],
           [[16.47,90.0],[17.91,30.33]], [[22.86,77.74],[25.06,26.26]],
           [[30.43,68.64],[33.44,23.20]],[[39.19,61.57],[43.05,20.81]],
           [[47.81,90.0],[49.16,55.92],[53.88,18.89]]]

    dpa = [np.array([[x[0]*np.cos(a2r*x[1]),x[0]*np.sin(a2r*x[1])] for x in y]) for y in dpa]

    # Table 3 Blanch & Clemm for b_r 1<=r<=9 (b_0 is invalid, and b_1 has no first-quadrant zeros)
    dpb = [[[np.NaN,np.NaN],], [[6.93,90.0],], [[11.27,72.46],], [[16.80,60.98],], 
           [[23.53,52.83],], [[30.10,90.0],[31.47,46.71]], [[38.52,80.58],[40.62,41.95]],
           [[48.14,73.09],[50.99,38.12]], [[58.94,66.97],[62.57,34.98]]]

    dpb = [np.array([[x[0]*np.cos(a2r*x[1]),x[0]*np.sin(a2r*x[1])] for x in y]) for y in dpb]

    shrnk = 0.65

    for k in range(max_mcn):
        plt.figure(figsize=(10,8))
        plt.subplot(221)
        plt.imshow(mcn_a[:,:,k].real,extent=ax,interpolation='nearest')
        plot_a_double_points(dpb,k)
        plt.title('$\\Re(a_%i)$' % k)
        plt.ylabel('$\\Im(q)$')
        plt.colorbar(shrink=shrnk)
        plt.axis('tight')
        
        plt.subplot(222)
        plt.imshow(mcn_a[:,:,k].imag,extent=ax,interpolation='nearest')
        plot_a_double_points(dpb,k)
        plt.title('$\\Im(a_%i)$' % k)
        plt.colorbar(shrink=shrnk)
        plt.axis('tight')
        
        plt.subplot(223)
        plt.imshow(np.abs(mcn_a[:,:,k]),extent=ax,interpolation='nearest')
        plot_a_double_points(dpb,k)
        plt.title('$|a_%i|$' % k)
        plt.xlabel('$\\Re(q)$')
        plt.ylabel('$\\Im(q)$')
        plt.colorbar(shrink=shrnk)
        plt.axis('tight')

        plt.subplot(224)
        plt.imshow(np.arctan2(mcn_a[:,:,k].imag,mcn_a[:,:,k].real),
                   extent=ax,interpolation='nearest')
        plot_a_double_points(dpb,k)
        plt.title('$\\arg(a_%i)$' % k)
        plt.xlabel('$\\Re(q)$')
        plt.colorbar(shrink=shrnk)
        plt.axis('tight')
        plt.savefig('mcn_check_a%i.png' % k)
        plt.subplots_adjust(left  = 0.1,   right = 0.95, 
                            bottom = 0.05,  top = 0.95,  
                            wspace = 0.20,  hspace = 0.10)

    for k in range(max_mcn):
        plt.figure(figsize=(10,8))
        plt.subplot(221)
        plt.subplots_adjust(left  = 0.1,   right = 0.95, 
                            bottom = 0.05,  top = 0.95,  
                            wspace = 0.20,  hspace = 0.10)
        plt.imshow(mcn_b[:,:,k].real,extent=ax,interpolation='nearest')
        plot_b_double_points(dpb,k)
        plt.title('$\\Re(b_%i)$' % (k+1))
        plt.ylabel('$\\Im(q)$')
        plt.colorbar(shrink=shrnk)
        plt.axis('tight')
    
        plt.subplot(222)
        plt.imshow(mcn_b[:,:,k].imag,extent=ax,interpolation='nearest')
        plot_b_double_points(dpb,k)
        plt.title('$\\Im(b_%i)$' % (k+1))
        plt.colorbar(shrink=shrnk)
        plt.axis('tight')

        plt.subplot(223)
        plt.imshow(np.abs(mcn_b[:,:,k]),extent=ax,interpolation='nearest')
        plot_b_double_points(dpb,k)
        plt.title('$|b_%i|$' % (k+1))
        plt.xlabel('$\\Re(q)$')
        plt.ylabel('$\\Im(q)$')
        plt.colorbar(shrink=shrnk)
        plt.axis('tight')

        plt.subplot(224)
        plt.imshow(np.arctan2(mcn_b[:,:,k].imag,mcn_b[:,:,k].real),
                   extent=ax,interpolation='nearest')
        plt.title('$\\arg(b_%i)$' % (k+1))
        plt.xlabel('$\\Re(q)$')
        plt.colorbar(shrink=shrnk)
        plot_b_double_points(dpb,k)
        plt.axis('tight')
        plt.savefig('mcn_check_b%i.png' % (k+1))


# #########################################
# #########################################
# plot curves of mcn for real q (for checking / comparison)
# figures 8a and 8b in McLachlan pages 40 & 41

# same for both normalizations
q = np.linspace(-40.0,40.0,75)

mcn_a = np.empty((len(q),max_mcn))
mcn_b = np.empty((len(q),max_mcn))

for i in range(len(q)):
    m = mf.mathieu(q=q[i]+0.0j, M=50)
        
    for k in range(max_mcn/2):
            
        # starts at a_0 -> ce_0
        mcn_a[i,2*k]   = m.mcn[k,0] # correct
        mcn_a[i,2*k+1] = m.mcn[k,1]
            
        # starts at b_1 -> se_1
        mcn_b[i,2*k]   = m.mcn[k,3]
        mcn_b[i,2*k+1] = m.mcn[k,2]

# colors
c = ['r','g','b','k','m','c','y','g']
aleg = ['$a_%i$'%n for n in range(max_mcn)]
bleg = ['$b_%i$'%n for n in range(1,max_mcn+1)]

plt.figure(figsize=(10,8))
for k in range(max_mcn):
    plt.plot(q,mcn_a[:,k],c[k]+'-',label=aleg[k])
    plt.plot(q,mcn_b[:,k],c[k]+'--',label=bleg[k])

plt.axis([-40,40,-15,80])
plt.xlabel('$q$')
plt.ylabel('mcn')
plt.title('Mathieu characteristic number')
plt.legend(loc='best')
plt.savefig('mcn_real_q.eps')


