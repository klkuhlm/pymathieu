/*            Standard include libraries     */
#include   <stdio.h>
#include   <stdlib.h>
#include   <iostream>
#include   <math.h>

/*       Include libraries  part of Mathieu functions Library  */
#include   "mathur.h"
#include   "bsslr.h"
#include   "mmathur.h"
#include   "mbsslr.h"

using namespace std;

/*
  The Author would be happy to help in any way he can
  in the use of these routins.

  Contact Address:
  CERI, KACST, P.O. Box 6086,
  Riyadh 11442,
  Saudi Arabia,
  Fax:966+1+4813764
  Email: alhargan@kacst.edu.sa

  Legal Matters:
  The program is here as is, no guarantees are given, stated or implied,
  you may use it at your own risk.

  Last modified 20/5/1999

  This a driver for the Mathieu functions library
  The following files must be compiled and linked:
  MATHUR.CPP     Mathieu functions
  MMATHUR.CPP    Modified Mathieu functions
  MCNR.CPP       Mathieu charactristic Numbers
  BSSLR.CPP      Bessel functions
  MBSSLR.CPP     Modified Bessel functions
*/

/* KK: Modified code to compile with modern C++ compiler (changed
   <iostream.h> to <iostream> and added "using namespace std;") and 
   modified this routine to produce results
   in a format more comparable with my Python program */

/*----------------- Main Porgram --------------------------*/
int  main (void)
{
  int n,M,nsteps,minOrd;
  double h,v,radRange,angRange,pi;
  char typ,r;
  short kind;
  pi=4*atan(1.0);

  cout <<"\n (e)ven or (o)dd: ";
  cin >>typ;
  cout <<" max order n: ";
  cin >> M;
  cout <<" Circumferential(c) or Radial(r): ";
  cin >>r;
  cout <<" kind: First(1) Second(2): ";
  cin >>kind;
  cout <<" Mathieu parameter, h: ";
  cin >>h;

  // sensible defaults?
  radRange = 3.0;
  angRange = 2*pi;
  nsteps = 200;

  if(typ=='e')
    {minOrd = 0;} 
  else 
    {minOrd = 1;}

  if(r=='r')
    {
      for(v=0; v<=radRange; v+=radRange/nsteps)
	{
	  printf("\n%.14e ",v);
	  for(n=minOrd; n<=M; n+=1)
	    {
	      printf("%.14e ",MathuZn(typ,n,h,v,kind)); 
	    }
	}
    }
  else
    {
      for(v=0; v<=angRange; v+=angRange/nsteps)
	{
	  printf("\n%.14e ",v);
	  
	  for(n=minOrd; n<=M; n+=1)
	    {
	      printf("%.14e ",MathuSn(typ,n,h,v,kind));
	    }
	}
    }

  printf("\n");
  exit(0);
}  // End Main
