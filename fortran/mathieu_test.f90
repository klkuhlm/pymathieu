! $Id: mathieu_test.f90,v 1.3 2007/06/27 16:18:06 kris Exp kris $

program mathieu_test

  use constants, only : DP, PI
  use mcn_matrix_method, only : mcn_eigenvalues
  use mathieu_functions, only : mmatce, mmatse, mmatKeKo, mmatDKeDKo, &
       & mmatDce, mmatDse, mmatDIeDIo, mmatIeIo
  use shared_mathieu, only : A,B,q

  implicit none
  integer, parameter :: LENE = 200, LENP = 200, MAXORD = 10
  character(2) :: chnum
  character(35) :: fmt
  character(10) :: suffix
  character(11), parameter :: fmt1 = '(ES12.4,1x,'
  character(21), parameter :: fmt2 = '(ES12.4,1x,ES12.4,1x)'
  character(3), dimension(12) :: type
  real(DP), dimension(1:LENP) :: etaMK, etaMI
  real(DP), dimension(1:LENE) :: psiM
  complex(DP), dimension(0:MAXORD,1:LENP,1:4) :: aout
  complex(DP), dimension(0:MAXORD,1:LENE,5:12) :: rout
  integer :: i,j,k, MS
  integer, dimension(0:MAXORD), parameter :: vi = (/ (i, i=0,MAXORD) /)
  real(DP) :: rq,iq

  type = (/'ce ','Dce','se ','Dse','Ke ','DKe','Ko ','DKo','Ie ',&
       & 'DIe','Io ','DIo'/)
  write(chnum,'(I2)') MAXORD+1
  fmt = fmt1//chnum//fmt2//')'
  write(chnum,'(I2)') (MAXORD+1)**2/2+1

  open(unit=77,file='mathieu_for_plotting.in')
  read(77,*) rq,iq
  read(77,*) suffix
  close(77)

  etaMK = (/0.0_DP, logspace(-3.0,log10(2.0),LENE-1)/)
  etaMI = linspace(0.0_DP,1.0_DP,LENP)
  psiM = linspace(0.0_DP,PI,LENP)

  ! functions are defined for negative of this q; "modified"
  q = cmplx(rq,iq,DP)   
  ms = max(20,2*MAXORD,int(2.0*abs(q)))

  print *, 'calculating MF for q=',q
  print *, 'using matrix size MS=',MS

  allocate(A(1:MS,0:MS-1,1:2),B(1:MS,0:MS-1,1:2))

  call mcn_eigenvalues(q,A,B,1) ! 1 = McLachlan, 2 = Stratton

  ! compute angular functions
  !========================================
  aout(0,1:LENP,1) = mmatce(0,psiM(1:LENP))
  aout(0,1:LENP,2) = mmatDce(0,psiM(1:LENP))

  do i = 1,MAXORD
     aout(i,1:LENP,1) = mmatce(i,psiM(1:LENP))
     aout(i,1:LENP,2) = mmatDce(i,psiM(1:LENP))
     aout(i,1:LENP,3) = mmatse(i,psiM(1:LENP))
     aout(i,1:LENP,4) = mmatDse(i,psiM(1:LENP))
  end do

  ! output modified angular mathieu functions for plotting
  !========================================
  do k = 1, 4
     open(unit=20,file=trim(type(k))//'_bench.out',&
          & status='replace',action='write')
     write(20,*) '# Mathieu functions for q=',q
     write(20,*) '# ',type(k)
     do j = 1, LENP
        write(20,fmt) psiM(j),(real(aout(i,j,k)),aimag(aout(i,j,k)),i=0,MAXORD)
     end do
     close(20)
  end do

  ! compute radial functions
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  do i = 1,LENE
     call mmatKeKo(vi(0:MAXORD),etaMK(i),rout(0:MAXORD,i,5),rout(0:MAXORD,i,7))
     call mmatDKeDKo(vi(0:MAXORD),etaMK(i),rout(0:MAXORD,i,6),rout(0:MAXORD,i,8))
     call mmatIeIo(vi(0:MAXORD),etaMI(i),rout(0:MAXORD,i,9),rout(0:MAXORD,i,11))
     call mmatDIeDIo(vi(0:MAXORD),etaMI(i),rout(0:MAXORD,i,10),rout(0:MAXORD,i,12))
  end do

  ! output first and second kind modified radial mathieu functions for plotting
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  do k = 5, 8
     open(unit=20,file=trim(type(k))//'_bench'//trim(suffix)//'.out',&
          & status='replace',action='write')
     write(20,*) '# K Mathieu functions for q=',q
     write(20,*) '# ',type(k)
     do j = 1, LENE
        write(20,fmt) etaMK(j),(real(rout(i,j,k)),aimag(rout(i,j,k)),i=0,MAXORD)
     end do
     close(20)
  end do
  
  do k = 9, 12
     open(unit=20,file=trim(type(k))//'_bench'//trim(suffix)//'.out',&
          & status='replace',action='write')
     write(20,*) '# I Mathieu functions for q=',q
     write(20,*) '# ',type(k)
     do j = 1, LENE
        write(20,fmt) etaMI(j),(real(rout(i,j,k)),aimag(rout(i,j,k)),i=0,MAXORD)
     end do
     close(20)
  end do

  ! output NORMALIZED first and second kind modified radial mathieu functions for plotting
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !! K-type radial functions normalized by their value at eta=0
  do k = 5, 8
     open(unit=20,file=trim(type(k))//'_norm_bench.out',status='replace',action='write')
     write(20,*) '# normalized K Mathieu functions for q=',q
     write(20,*) '# ',type(k)
     do j = 1, LENE
        write(20,fmt) etaMK(j),(real(rout(i,j,k)/rout(i,1,k)),&
             & aimag(rout(i,j,k)/rout(i,1,k)),i=0,MAXORD)
     end do
     close(20)
  end do

!!$  ! output line source normalized functions
!!$  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!!$  !! even order even K-Mathieu functions normalized by their derivative at eta=0
!!$  open(unit=20,file='flux_source_bench.out',status='replace',action='write')
!!$  write(20,*) '# Mathieu functions for q=',q
!!$  write(20,*) '# line source functions Ke_{2n}(eta)/DKe_{2n}(0)'
!!$  do j = 1, LENE
!!$     write(20,fmt) etaMK(j),(real(rout(2*i,j,5)/rout(2*i,1,6)),&
!!$          & aimag(rout(2*i,j,5)/rout(2*i,1,5)),i=0,floor(MAXORD/2.0))
!!$  end do
!!$  close(20)

  deallocate(A,B)

contains

  function logspace(bot,top,num) result(v)
    use constants, only : DP
    real, intent(in) :: bot,top
    integer, intent(in) :: num
    real(DP), dimension(num) :: v

    v = 10.0_DP**linspace(real(bot,DP),real(top,DP),num)

  end function logspace
  

  ! ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  function linspace(lo,hi,num) result(v)
    use constants, only : DP
    real(DP), intent(in) :: lo,hi
    integer, intent(in) :: num
    real(DP), dimension(num) :: v

    integer :: i
    real(DP) :: rnum, range

    if(lo >= hi) stop "LINSPACE: lower bound must be less than upper bound."

    rnum = real(num - 1,DP)
    range = hi - lo

    v = (/ ( lo + real(i,DP)*range/rnum, i=0,num-1) /)

  end function linspace

end program mathieu_test

  
