from os import system
from glob import glob

def driver(cmd):
    print('running',cmd,'...')
    failure = system(cmd)
    if failure:
        print('error running '+ cmd)

# make this false if the files exist already
# and you just want to re-build the latex file
execute = True

fout = open('mathieu_test.tex','w')

# header
fout.write(r"""\documentclass[a4]{article}
\usepackage{epsfig,fullpage}
\renewcommand\floatpagefraction{0.95}
\renewcommand\topfraction{0.95}
\renewcommand\bottomfraction{.95}
\renewcommand\textfraction{.01}   
\setcounter{totalnumber}{50}
\setcounter{topnumber}{50}
\setcounter{bottomnumber}{50}
\begin{document}
""")

##-###############################
if execute:
    driver('python mathieu_plot.py')

fout.write(r"""\section{Plots of modified Mathieu functions and Derivatives}
\subsection{Constant real $q$, variable order}
""")

for fcn in ['ce','se','Ie','Io','Ke','Ko']:
    for fig in glob("order_range_q*_norm[1-2]_"+fcn+".eps"):
        fout.write('\\epsfig{file=%s,height=0.46\\textheight}\\\\\n' % fig)

fout.write('\\newpage\n\\subsection{Constant order, variable real $q$}')
for efcn,ofcn in zip(['ce','Ie','Ke',],['se','Io','Ko']):
    for ord in range(5):
        for fig in glob("q_range_norm[1-2]_"+efcn+"_n"+str(ord)+".eps"):
            fout.write('\\epsfig{file=%s,height=0.46\\textheight}\\\\\n' % fig)
        for fig in glob("q_range_norm[1-2]_"+ofcn+"_n"+str(ord+1)+".eps"):
            fout.write('\\epsfig{file=%s,height=0.46\\textheight}\\\\\n' % fig)

##############################
if execute:
    driver('python mathieu_plot_complexq.py')

fout.write('\\newpage\n\\subsection{Constant order, variable complex $q$}\n')
fout.write('$\\Re=$solid, $\\Im=$dashed: $q=x+xi, \\quad x\\in[0.01,0.1,1.0,10.0]$\\\\')
for efcn,ofcn in zip(['ce','Ie','Ke',],['se','Io','Ko']):
    for ord in range(3):
        for fig in glob("cmplx_q_range_norm[1-2]_"+efcn+"_n"+str(ord)+".eps"):
            fout.write('\\epsfig{file=%s,height=0.46\\textheight}\\\\\n' % fig)
        for fig in glob("cmplx_q_range_norm[1-2]_"+ofcn+"_n"+str(ord+1)+".eps"):
            fout.write('\\epsfig{file=%s,height=0.46\\textheight}\\\\\n' % fig)

##############################
if execute:
    driver('python mathieu_alhargan_compare.py')

fout.write('\\newpage\n\\subsection{Compare these functions to Alhargan''s for real $q$}\n')
for fig in glob("alhargan_compare_*_ang.eps"):
    fout.write('\\epsfig{file=%s,width=\\textwidth}\\\\\n' % fig)
for fig in glob("alhargan_compare_*_radial.eps"):
    fout.write('\\epsfig{file=%s,height=0.48\\textheight}\\\\\n' % fig)

##############################
if execute:
    driver('python mathieu_deriv_check.py')

fout.write('\\newpage\n\\section{Check derivative functions}\n')
fout.write(r"Each derivative function is compared with a finite-difference approximation of the derivative of the original function, with variable step size.  A straight line in log-space indicates the finite-difference approximation error is the controlling error between the two approaches.  The maximum absolute error across a range of arguments is plotted against finite-difference step size.  The different lines represent functions of different order (higher order functions having errors greater by a constant in log space).\\" + '\n')

for norm in [1,2]:
    fout.write('\\epsfig{file=angular_deriv_max_'+str(norm)+'_check.eps,height=0.4\\textheight}\\\\\n')

for norm in [1,2]:
    fout.write('\\epsfig{file=radial_deriv_max_'+str(norm)+'_check.eps,height=0.8\\textheight}\\\\\n')

############################
if execute:
    driver('python mathieu_norm_check.py')

fout.write('\\newpage\n\\section{eigenvector normalization check}\n')
fout.write('For comparison with figures in chapter 2 of McLachlan.  Each figure shows the variation in the coefficients as $q$ is real and changes for both normalization schemes, and consistency overa a wide range of $q$.  The colors are ordered 0:red,1:green,2:blue,3:black,4:magenta,5:cyan,6:yellow, and are consistent across all the figures.\n')
for ord in range(6):
    fout.write('\\epsfig{file=ce%i_coeff_fcn_q.eps,width=0.75\\textwidth}\\\\\n' % ord)
    fout.write('\\epsfig{file=se%i_coeff_fcn_q.eps,width=0.75\\textwidth}\\\\\n' % (ord+1))

############################
if execute:
    driver('python mathieu_ode_deriv_check.py')

fout.write('\\newpage\n\\section{Mathieu ODE check}\n')
fout.write('Each function is substituted back into its respective ODE to see if the error in the result scales with the finite difference stepsize used to approximate the second derivative in terms of the first derivative functions.  Each function shows the the maximum error each of the different order functions has in their ODE over a range of input parameters $(0-2\\pi$ for angular and $0-2.5$ for radial$)$\\\\\n')
for ord in range(11):
    fout.write('\\epsfig{file=ode_angular_deriv_n2_q%i.eps,width=0.75\\textwidth}\\\\\n' % ord)
    if ord > 0:
        fout.write('\\epsfig{file=ode_radial_deriv_n2_q%i.eps,width=0.75\\textwidth}\\\\\n' % ord)


##############################
if execute:
    driver('python mathieu_mcn_check.py')

# since the mcn_figures are png (and they really are images, not suitable
# for eps, this part is done with pdflatex

fmcnout = open('mathieu_test_mcn.tex','w')
fmcnout.write(r"""\documentclass[a4]{article}
\usepackage{graphicx,fullpage}
\renewcommand\floatpagefraction{0.95}
\renewcommand\topfraction{0.95}
\renewcommand\bottomfraction{.95}
\renewcommand\textfraction{.01}   
\setcounter{totalnumber}{50}
\setcounter{topnumber}{50}
\setcounter{bottomnumber}{50}
\begin{document}
""")

fmcnout.write('These figures show the Mathieu Characteristic Number (MCN) as a function of the complex Mathieu parameter $q$.  The numerical values for the double points are taken from Blanch and Clemm (Math. of Comp., 1969).  Stars indicate the double points assigned to the mcn that the figure is titled after; circles indicate double points shared with an mcn two orders away.  The branch cuts assocated with the branch points are different than those in the large handbook reference by Blanch and Clemm, but they are a determined by LAPACK, which does the eigenvalue analysis.\\\\\n')
for n in range(8):
    fmcnout.write('\\includegraphics{mcn_check_a'+str(n)+'.png}\\\\\n')
    fmcnout.write('\\includegraphics{mcn_check_b'+str(n+1)+'.png}\\\\\n')

fmcnout.write('\\end{document}\n')
fmcnout.close()
print('building PDFLaTeX')
failure = system('pdflatex mathieu_test_mcn.tex > pdflatex.screen')
if failure:
    print('error running PDFLaTeX')

fout.write('\\newpage\n\\section{Mathieu MCN check}\\subsection{Real $q$}\n')
fout.write('This shows how the Mathieu characteristic numbers vary as a function of the Mathieu parameter $q$, for real $q$.  This figure in one form or another (or just the $q\\ge0$ portion) is shown in most books or papers with coverage of Mathieu functions.  The following expressions describe the symmetry in the figures $a_{2m}(-q)=a_{2m}(q)$; $b_{2m+2}(-q)=b_{2m+2}(q)$; $a_{2m+1}(-q)=b_{2m+1}(q)$ (see Blanch and Clemm, 1969). Modified Mathieu functions correspond to the left half of this figure.\\\\\n')
fout.write('\\epsfig{file=mcn_real_q.eps,width=0.9\\textwidth}\\\\\n')

# wrap it up
fout.write("\\end{document}\n")
fout.close()    

# build with LaTeX
print('building output with LaTeX ...')
failure = system('latex mathieu_test.tex > latex.screen')
if failure:
    print('error running LaTeX')

print('converting dvi to pdf ...')
failure = system('dvipdf mathieu_test.dvi')
if failure:
    print('error running dvipdf')
    
print('joining two pdfs together ...')
failure = system('pdftk mathieu_test.pdf mathieu_test_mcn.pdf cat output mathieu_test_results.pdf')
if failure:
    print('error running pdftk')
