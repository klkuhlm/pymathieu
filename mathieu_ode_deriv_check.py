import numpy as np
import mathieu_functions as mf
import matplotlib.pyplot as plt
from mathieu_deriv_check import fd_deriv as deriv

def ang_ode(f,d,m,n,z,q,dx):
    """ angular ODE: modified Mathieu equation
    McLachlan, section 2.18 page 21
    y" + (a + 2q cos(2z))*y = 0 """
    return deriv(d,n,z,dx=dx) + (m + 2*q*np.cos(2.0*z))*f(n,z)

def rad_ode(f,d,m,n,z,q,dx):
    """ radial ODE: modified Mathieu equation
    McLachlan, section 13.30 page 248
    y" - (a + 2q cosh(2z))*y = 0 """
    return deriv(d,n,z,dx=dx) - (m + 2*q*np.cosh(2.0*z))*f(n,z)

numq = 11
numdx = 12

re = np.linspace(0.0,10.0,num=numq)

psi = np.linspace(0,2*np.pi)
eta = np.linspace(0,2.0)

n = np.arange(6)

dxv = np.logspace(-8,-2,num=numdx)

aevrhs = np.empty((n.shape[0],numdx,numq),dtype=complex)
aodrhs = np.empty((n.shape[0],numdx,numq),dtype=complex)
revrhs = np.empty((n.shape[0],numdx,numq),dtype=complex)
rodrhs = np.empty((n.shape[0],numdx,numq),dtype=complex)

norm = 2

for i,q in enumerate(re):
    m = mf.mathieu(q=q,norm=norm)

    mcn_ce = np.empty((2*m.mcn.shape[0],),dtype=complex)
    mcn_se = np.empty((2*m.mcn.shape[0],),dtype=complex)
            
    for k in range(m.mcn.shape[0]):
        mcn_ce[2*k]   = m.mcn[k,0]  # a_2n
        mcn_ce[2*k+1] = m.mcn[k,3]  # b_2n+1
        
        mcn_se[2*k]   = m.mcn[k,1]  # a_2n+1  
        mcn_se[2*k+1] = m.mcn[k,2]  # b_2n+2


    ########################################
    ## Angular modified Mathieu functions
    ########################################

    for l,dx in enumerate(dxv):
        for j,nv in enumerate(n):            
            aevrhs[j,l,i] =     np.amax(np.abs(ang_ode(m.ce,m.dce, mcn_ce[j], nv,psi,m.q, dx)))
        for j,nv in enumerate(n[1:]):
            aodrhs[j,l,i] = np.amax(np.abs(ang_ode(m.se,m.dse, mcn_se[j], nv,psi,m.q, dx)))    
    
    plt.figure(figsize=(7,7))
    plt.subplot(121)
    for ord in n:
        plt.plot(np.log10(dxv),np.log10(np.abs(aevrhs[ord,:,i])),
                 label='n:%i a:%.1f'%(ord,mcn_se[ord]))
    plt.title('ce; $q=$'+ str(q)+'+0j; norm='+str(norm))
    plt.ylabel('$\\log_{10}[max(|$ode residual$|)]$')
    plt.xlabel('$\\log_{10}(\\Delta x)$')
    plt.axis('tight')
    plt.legend(loc='best')

    plt.subplot(122)
    for ord in n[1:]:
        plt.plot(np.log10(dxv),np.log10(np.abs(aodrhs[ord,:,i])),
                 label='n:%i a:%.1f'%(ord,mcn_se[ord-1]))
    plt.title('se; $q=$'+ str(q)+'+0j; norm='+str(norm))
    plt.xlabel('$\\Delta x$')
    plt.axis('tight')
    plt.legend(loc='best')
    plt.savefig('ode_angular_deriv_n'+str(norm)+'_q'+str(i)+'.eps')


    ########################################
    ## Radial modified Mathieu functions
    ########################################
    
    for l,dx in enumerate(dxv):
        for j,nv in enumerate(n):            
            revrhs[j,l,i] = np.amax(np.abs(rad_ode(m.Ie,m.dIe, mcn_ce[j], nv,eta,m.q, dx)))
        for j,nv in enumerate(n[1:]):
            rodrhs[j,l,i] = np.amax(np.abs(rad_ode(m.Io,m.dIo, mcn_se[j], nv,eta,m.q, dx)))

    plt.figure(figsize=(7.5,9.0))
    plt.subplot(221)
    for ord in n:
        plt.plot(np.log10(dxv),np.log10(np.abs(revrhs[ord,:,i])),
                 label='n:%i a:%.1f'%(ord,mcn_ce[ord]))
    plt.title('$y^{\\prime \\prime} - (a + 2q \\cosh(2 \\eta))y$; Ie')
    plt.ylabel('$\\log_{10}(Ie)$')
    plt.axis('tight')
    plt.legend(loc='best')

    plt.subplot(222)
    for ord in n[1:]:
        plt.plot(np.log10(dxv),np.log10(np.abs(rodrhs[ord,:,i])),
                 label='n:%i a:%.1f'%(ord,mcn_se[ord-1]))
    plt.title('Io; $q=$'+ str(q)+'+0j; norm='+str(norm))
    plt.ylabel('$\\log_{10}(Io)$')
    plt.axis('tight')
    plt.legend(loc='best')

    for l,dx in enumerate(dxv):
        for j,nv in enumerate(n):            
            revrhs[j,l,i] = np.amax(np.abs(rad_ode(m.Ke,m.dKe, mcn_ce[j], nv,eta,m.q,dx)))
        for j,nv in enumerate(n[1:]):
            rodrhs[j,l,i] = np.amax(np.abs(rad_ode(m.Ko,m.dKo, mcn_se[j], nv,eta,m.q,dx)))

    plt.subplot(223)
    for ord in n:
        plt.plot(np.log10(dxv),np.log10(np.abs(revrhs[ord,:,i])),
                 label='n:%i a:%.1f'%(ord,mcn_ce[ord]))
    plt.xlabel('$\\log_{10}(\\Delta x)$')
    plt.ylabel('$\\log_{10}(Ke)$')
    plt.axis('tight')
    plt.legend(loc='best')


    plt.subplot(224)
    for ord in n[1:]:
        plt.plot(np.log10(dxv),np.log10(np.abs(rodrhs[ord,:,i])),
                 label='n:%i a:%.1f'%(ord,mcn_se[ord-1]))
    plt.xlabel('$\\log_{10}(\\Delta x)$')
    plt.ylabel('$\\log_{10}(Ko)$')
    plt.axis('tight')
    plt.legend(loc='best')

    plt.savefig('ode_radial_deriv_n'+str(norm)+'_q'+str(i)+'.eps')
         
