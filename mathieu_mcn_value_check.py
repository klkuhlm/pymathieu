import numpy as np
import mathieu_functions as mf

# for comparison with Appendix II in McLachlan's book (checks out exactly to 7 digits)
##################################################
qr = np.array([0.,1.,2.,3.,4.,5.,6.,7.,8.,9.,10,12.,14.,16.,18.,20.,24.,28.,32.,36.,40.])

max_mcn = 6

a = np.empty((len(qr),max_mcn),dtype=float)
b = np.empty((len(qr),max_mcn),dtype=float)

for i in range(qr.shape[0]):

    M = max(16,int(1.5*abs(qr[i])))
    m = mf.mathieu(q=qr[i],M=M)
            
    for k in range(max_mcn//2):
        # for mathieu characteristic numbers
        # mcn[:,j], j=0 a_{2n}    
        #           j=1 a_{2n+1}  
        #           j=2 b_{2n+2}  
        #           j=3 b_{2n+1}  
        
        # starts at a_0 -> ce_0
        a[i,2*k]   = m.mcn[k,0]
        a[i,2*k+1] = m.mcn[k,1]
        
        # starts at b_1 -> se_1
        b[i,2*k+1]= m.mcn[k,2]
        b[i,2*k]  = m.mcn[k,3]


# make tables of values like tables in book
toptab = np.concatenate((qr[:,None],-a[:,0:1],-b[:,0:1],-a[:,1:2],-b[:,1:2],a[:,2:3],b[:,2:3]),axis=1)
bottab = np.concatenate((qr[:,None],a[:,3:4],b[:,3:4],a[:,4:5],b[:,4:5],a[:,5:6],b[:,5:6]),axis=1)

tab = [toptab,bottab]

header = [' q |    -a0     |      -b1   |     -a1    |    -b2     |      +a2   |     +b3    |\n',
          ' q |     a3     |       b4   |      a4    |     b5     |       a5   |      b6    |\n']

f = open('mcn_mclachlan_table_comparison.dat','w')
f.write('numerical values for comparison with Appendix II of McLachlans Mathieu function book\n\n')

for t,h in zip(tab,header): 
    f.write(h)
    f.write('-'*82 + '\n')
    for j in range(t.shape[0]): # cycle over rows
        f.write('%2.0f |' % t[j,0])
        for i in range(max_mcn): # cycle over columns
            f.write('%11.7f |' % t[j,i+1])
        f.write('\n')
        if j in [0,5,10,15]:
            f.write('\n')  # extra blank lines every 5 rows
    f.write('\n\n')

f.close()

##################################################
##################################################
# for comparison with Muholland & Goldstein (Phil. Mag, 1929)

qi1 = np.array([0.02,0.04,0.06,0.08,0.1,0.12,0.14,0.16,0.18])
max_mcn = 4
a1 = np.empty((len(qi1),max_mcn),dtype=float)

for i in range(qi1.shape[0]):

    # Mulholland & Goldstein use a different Mathieu equation
    # y" + (4a - 16q cos(2x))*y = 0
    # so there is a factor of 8 here and 4 below
    m = mf.mathieu(q=8.0*(0.0+qi1[i]*1j))
            
    for k in range(max_mcn//2):
        a1[i,2*k]   = m.mcn[k,0]/4.0
        a1[i,2*k+1] = m.mcn[k,1]/4.0

tab1a = np.concatenate((qi1[:,None],a1[:,0:1],a1[:,2:3]),axis=1)
np.savetxt('mcn_goldstein_table1a_comparison.txt',tab1a,fmt='%.6f')

####################
# this seems to work, but the a0 values are conjugates of those in the table in the paper

qi2 = np.array([0.2,0.4,0.6,0.8,1.,1.2,1.4,1.6,1.8,2.])
max_mcn = 6
a2 = np.empty((len(qi2),max_mcn),dtype=complex)
b2 = np.empty((len(qi2),max_mcn),dtype=complex)

for i in range(qi2.shape[0]):

    m = mf.mathieu(q=8.0*(0.0+qi2[i]*1j))
            
    for k in range(max_mcn//2):
        a2[i,2*k]   = m.mcn[k,0]/4.0
        a2[i,2*k+1] = m.mcn[k,1]/4.0

        b2[i,2*k+1]= m.mcn[k,2]/4.0
        b2[i,2*k]  = m.mcn[k,3]/4.0

tab1b = np.concatenate((qi2[:,None],np.conj(a2[:,0:1]),b2[:,0:1],a2[:,3:4],b2[:,1:2],b2[:,3:4]),axis=1)

f = open('mcn_goldstein_table1b_comparison.txt','w')
f.write(' s           a0                  b1                  a3                  b2                  b4\n'+'-'*105+'\n')
for i in range(tab1b.shape[0]):  # cycle over rows
    f.write('%3.1f  '%tab1b[i,0])
    for j in range(1,tab1b.shape[1]):
        f.write('%8.6f%+9.6fi  ' % (tab1b[i,j].real, tab1b[i,j].imag))
    f.write('\n')
f.close()

# not sure exactly what is wrong with the second and last columns
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tab2 = np.concatenate((qi2[:,None],0.5*(np.conj(a2[:,0:1])+b2[:,0:1]),0.5*(a2[:,3:4]+b2[:,3:4]),
                       (b2[:,0:1]-np.conj(a2[:,0:1])),(b2[:,3:4]-a2[:,3:4])),axis=1)

f = open('mcn_goldstein_table2_comparison.txt','w')
for i in range(tab1b.shape[0]):  # cycle over rows
    f.write('%3.1f  '%tab2[i,0])
    for j in range(1,tab2.shape[1]):
        f.write('%8.6f%+9.6fi  ' % (tab2[i,j].real, tab2[i,j].imag))
    f.write('\n')
    if i == 4:
        f.write('\n')
f.close()
