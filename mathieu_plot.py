import numpy as np
import mathieu_functions as mf
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

tiny = 1.0E-15
part1 = True

def angular_plot_stuff(ylabel,title):
    plt.xlabel('$\\psi$')
    plt.xticks(np.linspace(0,np.pi/2.0,5),('$0$','$\\pi/8$','$\\pi/4$','$3\\pi/8$','$\\pi/2$'))
    plt.title(title)
    plt.ylabel(ylabel)
    plt.xlim([0,np.pi/2.0])

def radial_plot_stuff(ylabel,title):
    plt.xlabel('$\\sinh(\\eta)$')
    plt.ylabel(ylabel)
    plt.title(title)
    plt.xlim([0,np.sinh(np.max(eta))])


# ##################################################
# create plots of functions for ranges of argument and different orders
# all for a real-only value of q

ord = np.arange(6)
phi = np.linspace(0,np.pi/2.0,200)
eta = np.linspace(0,2.0,50)

if part1:
    # make a list of classes, one item for each q or norm
    mlist = []
    mlist.append(mf.mathieu(q=2.0+0.0j,norm=2))
    mlist.append(mf.mathieu(q=10.0+0.0j,norm=2))    
    mtitle = ['q2','q10']  
    
    for j,m in enumerate(mlist):
    
        # lists of angular funcitons and derivatives
        af = [m.ce, m.se]
        ad = [m.dce, m.dse]
    
        # plot angular functions
        for f,d in zip(af,ad):
            plt.figure(figsize=(10,6))
    
            # plot real part of functions on left, real part of derivs on right
            for n in ord:
                plt.subplot(121)
                plt.plot(phi,f(n,phi).real)
                plt.subplot(122)
                plt.plot(phi,d(n,phi).real)
            
            plt.subplot(121)
            angular_plot_stuff('angular Mathieu fcn','$%s_n(\\psi)$ q=%.2f'%(f.__name__,m.q.real))
            plt.subplot(122)
            angular_plot_stuff('','$\\partial/\\partial \\psi$ MF')

            plt.savefig('fcn_check_order_range_%s_%s.pdf' % (mtitle[j],f.__name__))
    
        # radial functions and derivatives
        rf = [m.Je, m.Jo, m.Ne, m.No]
        rd = [m.dJe,m.dJo,m.dNe,m.dNo]
    
        # plot radial functions
        for f,d in zip(rf,rd):
            print(f.__name__,end="")
            plt.figure(figsize=(10,6))
    
            # plot real part of functions on left, real part of derivs on right
            for n in ord:
                print(n,end="")
                plt.subplot(121)
                z = f(n,eta).real
                z[np.abs(z) < tiny] = 0.0
                if not np.all(np.isnan(z)):
                    # logscale does not handle NaN or negative numbers gracefully
                    plt.semilogy(np.sinh(eta),z)
                    plt.subplot(122)
                    z = d(n,eta).real
                    z[np.abs(z) < tiny] = 0.0
                    plt.semilogy(np.sinh(eta),np.abs(z))
    
            plt.subplot(121)
            radial_plot_stuff('radial Mathieu fcn','$%s_n(\\eta)$ q=%.2f'%(f.__name__,m.q.real))

            plt.subplot(122)
            radial_plot_stuff('','$|\\partial / \\partial \\eta$ MF $|$' )
    
            if f.__name__ == 'Jo' or f.__name__ == 'No':
                leg = plt.legend(['$%s_%i(\\eta)$' % (f.__name__,n) for n in ord[0:-1]],loc='best')
                for t in leg.get_texts():
                    t.set_fontsize('small')
            else:
                leg = plt.legend(['$%s_%i(\\eta)$' % (f.__name__,n) for n in ord[:]],loc='best')
                for t in leg.get_texts():
                    t.set_fontsize('small')

            plt.savefig('fcn_check_order_range_%s_%s.pdf' % (mtitle[j],f.__name__))

part2 = True
# ##################################################
# create plots of functions for ranges of argument and 
# different real-only value of q for the same order

if part2:
    af = ['m.ce', 'm.se']
    ad = ['m.dce', 'm.dse']
    
    rf = ['m.Je','m.Jo','m.Ne','m.No']
    rd = ['m.dJe','m.dJo','m.dNe','m.dNo']

    for norm in [2]:
        
        # make a list of classes, one item for each q
        mlist = []
        qr = [0.01,0.1,1.0,10.0,100.0]
    
        for val in qr:
            mlist.append(mf.mathieu(q=val+0.0j,norm=norm,M=45))
    
        # plot angular functions
        for f,d in zip(af,ad):
            for n in ord:
                if f[2:] == 'se' and n == 0: 
                    continue

                plt.figure(figsize=(10,6))
    
                for m in mlist:
                    # plot real part of functions on left, real part of derivs on right
                    plt.subplot(121)
                    plt.plot(phi,eval(f)(n,phi).real)
    
                    plt.subplot(122)
                    plt.plot(phi,eval(d)(n,phi).real)        
                
                plt.subplot(121)
                angular_plot_stuff('angular Mathieu fcn','$%s_%i(\\psi)$'% (f[2:],n))
                
                plt.subplot(122)
                angular_plot_stuff('','$\\partial/\\partial \\psi$ MF')
    
                leg = plt.legend(['$%s_%i(\\psi,%.2f)$' % (f[2:],n,q) for q in qr],loc='best')
                for t in leg.get_texts():
                    t.set_fontsize('small')

                plt.savefig('fcn_check_q_range_%s_n%i.pdf' %(f[2:],n))
        
        # plot radial functions
        for f,d in zip(rf,rd):
            for n in [0,1,2,3]:
                if (f[2:] == 'No' or f[2:] == 'Jo') and n == 0: 
                    continue

                plt.figure(figsize=(10,6))
    
                for m in mlist:
                    
                    # plot real part of functions on left, real part of derivs on right
                    plt.subplot(121)
                    z = eval(f)(n,eta).real
                    z[np.abs(z)< tiny] = 0.0
                    plt.semilogy(np.sinh(eta),z)
    
                    plt.subplot(122)
                    z = eval(d)(n,eta).real
                    z[np.abs(z)< tiny] = 0.0
                    plt.semilogy(np.sinh(eta),np.abs(z))        
                
                plt.subplot(121)
                radial_plot_stuff('radial Mathieu fcn','$%s_%i(\\eta)$' % (f[2:],n))
                
                plt.subplot(122)
                radial_plot_stuff('', '$|\\partial / \\partial \\eta$ MF $|$')
    
                leg = plt.legend(['$%s_%i(\\eta,%.2f)$' % (f[2:],n,q) for q in qr],loc='best')
                for t in leg.get_texts():
                    t.set_fontsize('small')
                plt.savefig('fcn_check_q_range_%s_n%i.pdf' % (f[2:],n))

