import numpy as np
import mathieu_functions as mf
import matplotlib.pyplot as plt

# $Id$

def fd_deriv(f,n,x,dx=0.0001):
    """ compute finite difference derivative
    of the function being passed in wrt x"""
    assert dx > 0.0

    return (f(n,x+dx) - f(n,x))/dx

# this module use used elsewhere for the above function only
if __name__ == '__main__':

    c = ['r-','g-','b-','k-','r:','g:','b:','k:']

    n = np.arange(8)
    psi = np.linspace(0,2*np.pi)
    eta = np.linspace(0,2)
    
    fa = ['m.ce', 'm.se']
    da = ['m.dce','m.dse']
    
    fr = ['m.Ie', 'm.Io', 'm.Ke', 'm.Ko']
    dr = ['m.dIe','m.dIo','m.dKe','m.dKo']
##-##    
##-##    # loop over functions, comparing finite difference of
##-##    # function with derivative specified via calculus
##-##    
##-##    qr = [0.01,0.1,0.3,1.0,3.0,10.0,30.0]
##-##    # loop over various values of q and norms
##-##    
##-##    for norm in [1,2]:
##-##        for val in qr:
##-##            m = mf.mathieu(q=val+0.0j,norm=norm,M=50)
##-##            
##-##
##-##            # check angular functions (range of n & z)
##-##            z = np.empty((len(n),len(psi),len(fa)))            
##-##            for (j,(f,d)) in enumerate(zip(fa,da)):
##-##                z[:,:,j] = fd_deriv(eval(f),n,psi) - eval(d)(n,psi)
##-##                
##-##            plt.figure()
##-##            plt.subplot(121)
##-##            for ord in n:
##-##                plt.plot(psi,z[ord,:],c[ord])
##-##            plt.title('ce deriv check')
##-##            plt.xlabel('$\\psi$')
##-##            plt.ylabel('fd - calculus')
##-##
##-##            plt.subplot(122)
##-##            for ord in n[1:]:
##-##                plt.plot(psi,z[ord,:],c[ord])
##-##            plt.title('se deriv check')
##-##            plt.xlabel('$\\psi$')
##-##            plt.savefig('angular_deriv_'+str(val)+'_'+str(norm)+'_check.eps')
##-##    
##-##            # check radial functions relative error(range of n & z)
##-##            z = np.empty((len(n),len(psi),len(fr)))            
##-##            for (j,(f,d)) in enumerate(zip(fr,dr)):
##-##                fv = eval(d)(n,eta)
##-##                z[:,:,j] = np.abs(fd_deriv(eval(f),n,eta) - fv)/fv
##-##                
##-##            plt.figure()
##-##            plt.subplot(221)
##-##            for ord in n:
##-##                plt.semilogy(eta,z[ord,:],c[ord])
##-##            plt.title('Ie deriv check')
##-##            plt.ylabel('|fd - calculus|/calculus')
##-##
##-##            plt.subplot(222)
##-##            for ord in n[1:]:
##-##                plt.semilogy(eta,z[ord,:],c[ord])
##-##            plt.title('Io deriv check')
##-##
##-##            plt.subplot(223)
##-##            for ord in n:
##-##                plt.semilogy(eta,z[ord,:],c[ord])
##-##            plt.title('Ke deriv check')
##-##            plt.xlabel('$\\eta$')
##-##            plt.ylabel('|fd - calculus|/calculus')
##-##
##-##            plt.subplot(224)
##-##            for ord in n[1:]:
##-##                plt.semilogy(eta,z[ord,:],c[ord])
##-##            plt.title('Ko deriv check')
##-##            plt.xlabel('$\\eta$')
##-##            plt.savefig('radial_deriv_'+str(val)+'_'+str(norm)+'_check.eps')


    
    # check if error -> 0 as dx -> 0
    ndx = 10
    dxv = np.logspace(-5,-1,ndx)

    for norm in [1,2]:
        
        m = mf.mathieu(q=2.0+0.0j,norm=norm,M=50)
    
        maxerr = np.empty((len(n),len(fa),ndx))
    
        for (j,(f,d)) in enumerate(zip(fa,da)):
    
            for k,dx in enumerate(dxv):
                
                # plot error max (across psi) as a function of dx
                
                err = fd_deriv(eval(f),n,psi,dx=dx) - eval(d)(n,psi)
                maxerr[:,j,k] = np.amax(np.abs(err),axis=1)
    
    
        # plot max error
        plt.close(1)
        plt.figure(1,figsize=(10,6))
        plt.subplot(121)
        for ord in n:
            plt.loglog(dxv,maxerr[ord,0,:],c[ord])
        plt.title('ce, max error, norm='+str(norm))
        plt.xlabel('$\\Delta x$ in fd approx')
        plt.ylabel('max(|fd - calculus|)')
        plt.axis('tight')
    
        plt.subplot(122)
        for ord in n[1:]:
            plt.loglog(dxv,maxerr[ord,1,:],c[ord])
        plt.title('se, max error')
        plt.xlabel('$\\Delta x$ in fd approx')
        plt.axis('tight')
        plt.savefig('angular_deriv_max_'+str(norm)+'_check.eps')
    
    
        maxerr = np.empty((len(n),len(fr),ndx))
    
        for (j,(f,d)) in enumerate(zip(fr,dr)):
    
            for k,dx in enumerate(dxv):
                
                # plot error max (across eta) as a function of dx
                err = fd_deriv(eval(f),n,eta,dx=dx) - eval(d)(n,eta)
                maxerr[:,j,k] = np.amax(np.abs(err),axis=1)
        
        # plot max error
        plt.close(1)
        plt.figure(1,figsize=(8,11))
        plt.subplot(221)
        for ord in n:
            plt.loglog(dxv,maxerr[ord,0,:],c[ord])
        plt.title('Ie, max error, norm='+str(norm))
        plt.ylabel('max(|fd - calculus|)')
        plt.axis('tight')
    
        plt.subplot(222)
        for ord in n[1:]:
            plt.loglog(dxv,maxerr[ord,1,:],c[ord])
        plt.title('Io, max error')
        plt.axis('tight')
    
        plt.subplot(223)
        for ord in n:
            plt.loglog(dxv,maxerr[ord,2,:],c[ord])
        plt.title('Ke, max error')
        plt.xlabel('$\\Delta x$ in fd approx')
        plt.ylabel('max(|fd - calculus|)')
        plt.axis('tight')
    
        plt.subplot(224)
        for ord in n[1:]:
            plt.loglog(dxv,maxerr[ord,3,:],c[ord])
        plt.title('Ko, max error')
        plt.xlabel('$\\Delta x$ in fd approx')
        plt.axis('tight')
        plt.savefig('radial_deriv_max_'+str(norm)+'_check.eps')
