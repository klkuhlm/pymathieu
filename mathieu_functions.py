# Copyright (c) 2010-2012 Kristopher L. Kuhlman (klkuhlm at sandia dot gov)
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
# 
# this Python library is for computing Mathieu functions of
# complex Mathieu parameter and integer order.  All functions require
# numpy version 1.3 or greater, while the radial MF require
# scipy (0.7 or better has fixed many bugs with the Bessel functions)
#
# as an example the library can be used in the following manner
# to plot the cosine-elliptic angular MF for complex q
# over the range 0 < psi < pi/2 and the orders 0 <= ord <= 7
#
# >> import numpy as np
# >> import matplotlib.pyplot as plt
# >> import mathieu_functions as mf
# >> m = mf.mathieu(2.0 + 5.0j)
# >> ord = np.arange(8)
# >> psi = np.linspace(0,np.pi/2,200)
# >> ce = m.ce(ord,psi)
# >> for n in ord:
# >>     plt.plot(psi,ce[n,:].real,'-')
# >>     plt.plot(psi,ce[n,:].imag,':')
# >> plt.xlabel('$\\psi$')
# >> plt.ylabel('ce$_{n}(\\psi,q=2+5i)$')
# >> plt.show()

import numpy as np

class mathieu(object):
    """Class containing all things related to modified Mathieu functions 
    (i.e.,  Mathieu functions of complex Mathieu parameter, both q>0 and q<0).

    Create an instance of the class to automatically compute the requisite
    eigenvectors of Mathieu coefficients for computing all Mathieu 
    functions.

    Mathieu functions are then methods of the mathieu class corresponding
    to the value of q used to initialize it."""

    class MathieuError(ValueError):
        pass

    def __init__(self, q, norm=2, M=20, maxOrd=20):
        """Determine the eigenvectors (A and B) necessary to compute
        Mathieu functions for the specified value of the Mathieu
        parameter (q).  The mcn is computed and saved, but not
        typically needed outside of testing/plotting purposes (it is
        not needed to compute Mathieu functions).

        The Stratton/Morse normalization is used, as it
        leads to simpler definitions of radial functions.
        
        required:    q :: scalar Mathieu parameter, non-zero, generally complex

        optional:    M :: size of infinite matrix 
                maxOrd :: maximum order of MF desired
                  norm :: normalization (choice 1 or 2) """

        q_test = np.asarray(q)
        if not q_test.shape == ():
            error = ('ERROR: only scalar values of q accepted, q.shape=' + 
                     str(q_test.shape))
            print(error)
            raise self.MathieuError

        # the complex Mathieu parameter
        if q.real < 0:
            print('use modified Mathieu function library for q<0')
            raise self.MathieuError

        self.q = np.complex128(q)

        # norm == 1 is Golstein-Ince normalization (used by McLachlan)
        # leads to angular functions with approximately unit max value

        # norm == 2 is Stratton-Morse-Chu normalization 
        # leads to simpler expressions for radial mathieu functions
        # and angular functions with either unit value or unit slope at psi=0
        
        if norm != 1 and norm != 2:
            print('ERROR: only valid norms are 1 and 2')
            raise self.MathieuError
        else:
            self.norm = int(norm)

        # the size of the "infinite" matrix used in eigenvalue calcs
        self.maxOrd = maxOrd
        self.M = max(self._shirts(),M)
        M = self.M

        # eigenvectors matricies used to compute 
        # Mathieu functions (q>0) (Alhargan calls them Be and Bo)
        self.A = np.empty((M,M,2),dtype=complex)
        self.B = np.empty((M,M,2),dtype=complex)

        coeff = np.zeros((M,M),dtype=complex)
        self.mcn = np.empty((M,4),dtype=complex)

        # A/B axis=0: subscript, index related to infinite sum
        # A/B axis=1: superscript, n in order=2n+1
        # A/B axis=2: 0(even) or 1(odd) 
        
        ord = np.arange(0,M+3)
        ford = np.double(ord)
        sgn = np.where(ord%2==0,1,-1)

        self._ord = ord     # vector of increasing integers
        self._ford = ford   # same vector as floats
        self._sgnord = sgn  # alternating sign vector
        self._rtpi2 = np.sqrt(np.pi/2.0)

        # even coefficents (A) of even order (De_{2n} in eq 3.12
        # J.J. Stamnes & B. Spjelkavik (Pure & Applied Optics, 1995))
        # also in Erricolo (ALG-861), 2006.
        ##################################################
        voff = np.ones((M-1,),dtype=complex)*q
        voffm =  np.diag(voff,k=-1) +  np.diag(voff,k=+1)
        coeff += np.diag(np.array((2*ford[0:M])**2,dtype=complex),k=0)
        coeff += voffm
        coeff[1,0] *= 2.0

        # compute eigenvalues (Mathieu characteristic numbers) and 
        # eigenvectors (Mathieu coefficients) 
        self.mcn[:,0],self.A[:,:,0] = np.linalg.eig(coeff)

        if self.norm == 1:
            # normalize to unit eigenvector length (first coeff included 2x)
            self.A[:,:,0] /= (self.A[0,:,0]*self.A[0,:,0] + 
                              np.sum(self.A[:,:,0]*self.A[:,:,0],axis=0))[None,:]
        else:
            # normalize so |ce_2n(psi=0)| = +1
            self.A[:,:,0] /= np.sum(self.A[:,:,0],axis=0)[None,:]

        # even coefficents (A) of odd order (De_{2n+1} in eq3.14 St&Sp)
        ##################################################
        coeff = 0.0
        coeff += np.diag(np.array((2*ford[0:M] + 1)**2,dtype=complex),k=0)
        coeff += voffm
        coeff[0,0] += q

        self.mcn[:,1],self.A[:,:,1] = np.linalg.eig(coeff)

        if self.norm == 1:
            # normalize to unit eigenvector length 
            self.A[:,:,1] /= np.sum(self.A[:,:,1]*self.A[:,:,1],axis=0)[None,:]
        else:
            # normalize so |ce_2n+1(psi=0)| = +1
            self.A[:,:,1] /= np.sum(self.A[:,:,1],axis=0)[None,:]

        # odd coefficents (B) of even order (Do_{2n+2} in eq3.16 St&Sp)
        ##################################################
        coeff = 0.0
        coeff += np.diag(np.array((2*ford[1:M+1] + 2)**2,dtype=complex),k=0)
        coeff += voffm
        
        self.mcn[:,2],self.B[:,:,0] = np.linalg.eig(coeff)

        if self.norm == 1:
            # normalize to unit eigenvector length
            self.B[:,:,0] /= np.sum(self.B[:,:,0]*self.B[:,:,0],axis=0)[None,:]
        else:
            # normalize so |se'_2n+2(psi=0)| = +1            
            self.B[:,:,0] /= (np.sum((2*ford[0:M,None] + 2)*
                                     self.B[:,:,0],axis=0)[None,:])

        # odd coefficents (B) of odd order (Do_{2n+1} in eq3.18 St&Sp)
        ##################################################
        coeff = 0.0
        coeff += np.diag(np.array((2*ford[0:M] + 1)**2,dtype=complex),k=0)
        coeff += voffm
        coeff[0,0] -= q
       
        self.mcn[:,3],self.B[:,:,1] = np.linalg.eig(coeff)
        
        if self.norm == 1:
            # normalize to unit eigenvector length
            self.B[:,:,1] /= np.sum(self.B[:,:,1]*self.B[:,:,1],axis=0)[None,:]
        else:
            # normalize so |se'_2n+1(psi=0)| = +1
            self.B[:,:,1] /= np.sum((2*ford[0:M,None] + 1)*
                                    self.B[:,:,1],axis=0)[None,:]

        # compute radial Mathieu function normalization (joining) factors
        ##################################################
        # square denominator term?
        sqrtq = np.sqrt(q)

        # ce_2n(psi=pi/2)
        self.p2n  = (np.sum(sgn[0:M,None]*self.A[:,:,0],axis=0) / 
                     self.A[0,:,0]**2)

        # ce'2n+1(psi=pi/2)
        self.p2n1 = (np.sum(-sgn[0:M,None]*(2*ford[0:M,None] + 1) * 
                             self.A[:,:,1],axis=0)/(sqrtq*self.A[0,:,1]**2))

        # se_2n+1(psi=pi/2)
        self.s2n1 = (np.sum(sgn[0:M,None]*(2*ford[0:M,None] + 1) * 
                            self.B[:,:,1],axis=0)/(sqrtq*self.B[0,:,1]**2))
        
        # se'_2n+2(psi=pi/2)
        self.s2n2 = (np.sum(-sgn[0:M,None]*(2*ford[0:M,None] + 2) * 
                             self.B[:,:,0],axis=0)/(q*self.B[0,:,0]**2))

        if norm == 1:
            # <for norm == 2 these are already unity>
            # ce_2n(psi=0)
            self.p2n  *= np.sum(self.A[:,:,0],axis=0)
            # ce_2n+1(psi=0)
            self.p2n1 *= np.sum(self.A[:,:,1],axis=0)
            # se'_2n+1(psi=0)
            self.s2n1 *= np.sum((2*ford[0:M,None] + 1)*self.B[:,:,1],axis=0)
            # se'_2n+2(psi=0)
            self.s2n2 *= np.sum((2*ford[0:M,None] + 2)*self.B[:,:,0],axis=0)
        
        # add extra dimension for vectorizing with respect to argument
        self.A.shape = (M,M,2,1)
        self.B.shape = (M,M,2,1)
        self.p2n.shape  = (M,1)
        self.p2n1.shape = (M,1)
        self.s2n1.shape = (M,1)
        self.s2n2.shape = (M,1)

    ##################################################
    ##################################################
    # internal utility functions used (leading single underscore)

    def _shirts(self):
        """estimate required matrix size to achieve accuracy ~ 1.0E-12,
        based on rational approximation due to  Shirts, R.B., 1993.
        "The Computation of Eigenvalues and Solutions of Mathieu's
        Differential Equation for Noninteger Order", ACM TOMS 19(3) p377-390.

        This was derived for real q, but seems to work for complex q"""
        n = self.maxOrd

        C = (8.46 +   0.444*n)/(1 + 0.085*n)
        D = (0.240 + 0.0214*n)/(1 + 0.059*n)
        return int(n + 3 + C*abs(self.q)**D)

    def _error_check(self,n):
        if np.max(n) > self.maxOrd:
            if self._shirts() <= self.M:
                # violating maximum order, but still OK
                return
            else:
                err = ("max Mathieu function requested (%i) is too high," +
                       "increase maxOrd parameter when initializing class" % 
                       np.max(n))
                print(err)
                raise self.MathieuError

    def _angFuncSetup(self,n,z):
        """return n & z as vectors
        return vector of floats (vv) and (-1)**ii
        return masks of even and odd terms in n"""
        self._error_check(n)
        ii = self._ord[0:self.M]
        vv = self._ford[0:self.M]
        nn = np.atleast_1d(np.int32(n)) # cast to int
        zz = np.atleast_1d(np.double(z)) # cast to double
        zs = zz.shape
        zz.shape = (-1,)
        return (nn,zz,vv,np.where(ii%2==0,1,-1)[:,None],nn%2==0,nn%2==1,zs)

    def _radFuncSetup(self,n,z):
        """return n & z as vectors
        return u / v arguments to Bessel functions
        return size of infinite matrix (M)
        return vector of integers (vv) and (-1)**vv
        return masks of even and odd terms in n"""
        self._error_check(n)
        sqrtq = np.sqrt(self.q)
        nn = np.atleast_1d(np.int32(n)) # cast to int
        zz = np.atleast_1d(np.double(z)) # cast to double
        zs = zz.shape
        zz.shape = (-1,)
        return (nn,zz,sqrtq*np.exp(-zz),sqrtq*np.exp(zz),self.M,nn%2==0,nn%2==1,zs)

    def _radDerivFuncSetup(self,n,z):
        """return n & z as vectors
        return sqrt(q), exp(u), exp(v), and u / v arguments to Bessel functions
        return size of infinite matrix (M)
        return vector of integers (vv) and (-1)**vv
        return masks of even and odd terms in n"""        
        self._error_check(n)
        sqrtq = np.sqrt(self.q)
        nn = np.atleast_1d(np.int32(n)) # cast to int
        zz = np.atleast_1d(np.double(z)) # cast to double
        zs = zz.shape
        zz.shape = (-1,)
        enz = np.exp(-zz)
        epz = np.exp(zz)
        return (nn,zz,sqrtq,enz,epz,sqrtq*enz,sqrtq*epz,self.M,nn%2==0,nn%2==1,zs)

    def _cleanup(self,y,zs):
        """reshape result vector to be conformant with passed-in argument"""
        newshape = [y.shape[0],]
        newshape.extend(zs)
        y.shape = newshape
        return np.squeeze(y)

    def _derivJY(self,z,W):
        """Compute derivatives of J & Y Bessel functions using recurrence
        without recomputing the functions again, assuming the derivatives 
        of order 0:n are needed from the functions of order 0:n
        http://dlmf.nist.gov/10.6
        
        z : complex argument vector
        W : Bessel function matrix (ord,arg)"""
            
        WD = np.empty_like(W)
        # n is highest order of Bessel fcn needed (starting at zero)
        n = W.shape[0] - 1  
    
        # three different recurrence relations used
        # 1) low end
        WD[0,:] = -W[1,:]
    
        # 2) middle
        WD[1:n-1,:] = 0.5*(W[0:n-2,:] - W[2:n,:])
    
        # 3) high end
        WD[n,:] = W[n-1,:] - n/z[None,:]*W[n,:]
        return WD

    ##################################################
    ##################################################
    ## radial Mathieu functions

    def ce(self,n,z):
        """even first-kind angular mathieu function (ce) 
        for scalar or vector orders or argument

        handles both q<0 : called Qe(q) by Alhargan
                 and q>0 : called Se(q) by Alhargan

        n: int modified Mathieu function order (scalar or vector)
        z: float angular argument to modified Mathieu function (periodic in
        at least 2*pi, so this is the logical range)"""

        n,z,v,vi,EV,OD,zs = self._angFuncSetup(n,z)
        y = np.empty((n.shape[0],z.shape[0]),dtype=complex)
        j = n[:]//2  # <- int(np.floor(n/2.0))

        y[EV,:] = np.sum(self.A[:,j[EV],0,:]*np.cos(np.outer(2*v,  z))[:,None,:],axis=0)
        y[OD,:] = np.sum(self.A[:,j[OD],1,:]*np.cos(np.outer(2*v+1,z))[:,None,:],axis=0)

        return self._cleanup(y,zs)

    def se(self,n,z):
        """odd first-kind angular mathieu function (se)
        for scalar or vector orders or argument
        
        handles both q<0: called Qo(q) by Alhargan
                 and q>0: called So(q) by Alhargan

        n: int modified Mathieu function order (scalar or vector)
        z: float angular argument to modified Mathieu function (periodic in
        at least 2*pi, so this is the logical range"""

        n,z,v,vi,EV,OD,zs = self._angFuncSetup(n,z)
        y = np.empty((n.shape[0],z.shape[0]),dtype=complex)
        j = (n[:]-1)//2  # se_0() invalid (set NaN below)

        y[EV,:] = np.sum(self.B[:,j[EV],0,:]*np.sin(np.outer(2*v+2,z))[:,None,:],axis=0)
        y[OD,:] = np.sum(self.B[:,j[OD],1,:]*np.sin(np.outer(2*v+1,z))[:,None,:],axis=0)

        y[n==0,:] = np.NaN
        return self._cleanup(y,zs)

    ##################################################
    ##################################################
    ## derivatives of radial Mathieu functions

    def dce(self,n,z):
        """even first-kind angular mathieu function derivative (Dce) 
        for scalar or vector orders or argument

        n: int modified Mathieu function order (scalar or vector)
        z: float angular argument to modified Mathieu function (periodic in
        at least 2*pi, so this is the logical range)"""

        n,z,v,vi,EV,OD,zs = self._angFuncSetup(n,z)
        vv = v[:,None,None]
        y = np.empty((n.shape[0],z.shape[0]),dtype=complex)
        j = n[:]//2 

        y[EV,:] = -np.sum( 2*vv*   self.A[:,j[EV],0,:]*np.sin(np.outer(2*v,  z))[:,None,:],axis=0)
        y[OD,:] = -np.sum((2*vv+1)*self.A[:,j[OD],1,:]*np.sin(np.outer(2*v+1,z))[:,None,:],axis=0)
    
        return self._cleanup(y,zs)

    def dse(self,n,z):
        """odd first-kind angular mathieu function derivative (Dse) 
        for scalar or vector orders or argument

        n: int modified Mathieu function order (scalar or vector)
        z: float angular argument to modified Mathieu function (periodic in
        at least 2*pi, so this is the logical range"""

        n,z,v,vi,EV,OD,zs = self._angFuncSetup(n,z)
        vv = v[:,None,None]
        y = np.empty((n.shape[0],z.shape[0]),dtype=complex)
        j = (n[:]-1)//2  # se_0() invalid

        y[EV,:] = np.sum((2*vv+2)*self.B[:,j[EV],0,:]*np.cos(np.outer(2*v+2,z))[:,None,:],axis=0)
        y[OD,:] = np.sum((2*vv+1)*self.B[:,j[OD],1,:]*np.cos(np.outer(2*v+1,z))[:,None,:],axis=0)

        y[n==0,:] = np.NaN
        return self._cleanup(y,zs)

    ##################################################
    ##################################################
    ## radial Mathieu functions for q>0

    def Je(self,n,z):
        """even first-kind radial Mathieu function (Je) for q>0
        analogous to J Bessel functions, called Ce in McLachlan p245 (eq 3&6),
        for scalar or vector orders or argument

        n: int order (scalar or vector)
        z: float radial argument (scalar or vector)"""

        from scipy.special import jv
        n,z,v1,v2,M,EV,OD,zs = self._radFuncSetup(n,z)
        y = np.empty((n.shape[0],z.shape[0]),dtype=complex)

        ord = self._ord[0:M+1]
        sgn = self._sgnord[0:M,None,None]
        J1 = jv(ord[0:M+1,None],v1[None,:])[:,None,:] # beta
        J2 = jv(ord[0:M+1,None],v2[None,:])[:,None,:] # alpha
        j = n[:]//2
        jsgn = np.where(j%2==0,1,-1)[:,None]

        y[EV,:] = (jsgn[j[EV],:]/self.A[0,j[EV],0] * 
                   np.sum(sgn*self.A[0:M,j[EV],0,:]*J1[0:M,:,:]*J2[0:M,:,:],axis=0))
        
        y[OD,:] = (jsgn[j[OD],:]/self.A[0,j[OD],1]*np.sum(sgn*self.A[0:M,j[OD],1,:] * 
                           (J1[0:M,:,:]*J2[1:M+1,:,:] + J1[1:M+1,:,:]*J2[0:M,:,:]),axis=0))

        return self._cleanup(y,zs)*self._rtpi2

    def Jo(self,n,z):
        """odd first-kind radial Mathieu function (Jo) for q>0
        analogous to J Bessel functions, called Se in McLachlan p245 (eq 5&7)
        for scalar or vector orders or argument

        n: int order (scalar or vector)
        z: float radial argument (scalar or vector)"""
        
        from scipy.special import jv
        n,z,v1,v2,M,EV,OD,zs = self._radFuncSetup(n,z)
        y = np.empty((n.shape[0],z.shape[0]),dtype=complex)

        ord = self._ord[0:M+2]
        sgn = self._sgnord[0:M,None,None]
        J1 = jv(ord[0:M+2,None],v1[None,:])[:,None,:]
        J2 = jv(ord[0:M+2,None],v2[None,:])[:,None,:]        
        j = (n[:]-1)//2  # Jo_0() invalid
        jsgn = np.where(j%2==0,1.0,-1.0)[:,None]

        y[EV,:] = (jsgn[j[EV],:]/self.B[0,j[EV],0]*np.sum(sgn*self.B[0:M,j[EV],0,:] *
                          (J1[0:M,:,:]*J2[2:M+2,:,:] - J1[2:M+2,:,:]*J2[0:M,:,:]),axis=0))

        y[OD,:] = (jsgn[j[OD],:]/self.B[0,j[OD],1]*np.sum(sgn*self.B[0:M,j[OD],1,:] *
                          (J1[0:M,:,:]*J2[1:M+1,:,:] - J1[1:M+1,:,:]*J2[0:M,:,:]),axis=0))

        y[n==0,:] = np.NaN
        return self._cleanup(y,zs)*self._rtpi2

    def Ne(self,n,z):
        """even second-kind radial Mathieu function (Ne) for q>0
        analogous to N or Y Bessel functions, called Fey in McLachlan p246-247 (eq 1&7) 
        for scalar or vector orders or argument

        n: int order (scalar or vector)
        z: float radial argument (scalar or vector)"""

        from scipy.special import jv,yv
        n,z,v1,v2,M,EV,OD,zs = self._radFuncSetup(n,z)
        y = np.empty((n.shape[0],z.shape[0]),dtype=complex)

        ord = self._ord[0:M+1]
        sgn = self._sgnord[0:M,None,None]
        J = jv(ord[0:M+1,None],v1[None,:])[:,None,:]
        N = yv(ord[0:M+1,None],v2[None,:])[:,None,:]
        j = n[:]//2
        jsgn = np.where(j%2==0,1.0,-1.0)[:,None]

        y[EV,:] = (jsgn[j[EV],:]/self.A[0,j[EV],0] * 
                   np.sum(sgn*self.A[0:M,j[EV],0,:]*J[0:M,:,:]*N[0:M,:,:],axis=0))
              
        y[OD,:] = (jsgn[j[OD],:]/self.A[0,j[OD],1]*np.sum(sgn*self.A[0:M,j[OD],1,:] *
                          (J[0:M,:,:]*N[1:M+1,:,:] + J[1:M+1,:,:]*N[0:M,:,:]),axis=0))

        return self._cleanup(y,zs)*self._rtpi2

    def No(self,n,z):
        """odd second-kind radial Mathieu function (No) for q>0 
        analogous to Y & N Bessel functions, called Gey in McLachlan p247 (eq 8&9)
        for scalar or vector orders or argument

        n: int order (scalar or vector)
        z: float radial argument (scalar or vector)"""
        
        from scipy.special import jv,yv
        n,z,v1,v2,M,EV,OD,zs = self._radFuncSetup(n,z)
        y = np.empty((n.shape[0],z.shape[0]),dtype=complex)

        ord = self._ord[0:M+2]
        sgn = self._sgnord[0:M,None,None]
        J = jv(ord[0:M+2,None],v1[None,:])[:,None,:]
        N = yv(ord[0:M+2,None],v2[None,:])[:,None,:]
        j = (n[:]-1)//2  
        jsgn = np.where(j%2==0,1.0,-1.0)[:,None]

        y[EV,:] = (jsgn[j[EV],:]/self.B[0,j[EV],0]*np.sum(sgn*self.B[0:M,j[EV],0,:] *
                          (J[0:M,:,:]*N[2:M+2,:,:] - J[2:M+2,:,:]*N[0:M,:,:]),axis=0))

        y[OD,:] = (jsgn[j[OD],:]/self.B[0,j[OD],1]*np.sum(sgn*self.B[0:M,j[OD],1,:] *
                          (J[0:M,:,:]*N[1:M+1,:,:] - J[1:M+1,:,:]*N[0:M,:,:]),axis=0))

        y[n==0,:] = np.NaN   # No_0() invalid
        return self._cleanup(y,zs)*self._rtpi2

    ##################################################
    ##################################################
    ## derivative radial Mathieu functions for q>0

    def dJe(self,n,z):
        """even first-kind radial Mathieu function derivative (dJe) for q>0
        analogous to J Bessel functions, called Ce in McLachlan, 
        for scalar or vector orders or argument

        n: int order (scalar or vector)
        z: float radial argument (scalar or vector)"""
        
        from scipy.special import jv
        n,z,sqrtq,enz,epz,v1,v2,M,EV,OD,zs = self._radDerivFuncSetup(n,z)
        dy = np.empty((n.shape[0],z.shape[0]),dtype=complex)

        ord = self._ord[0:M+1]
        sgn = self._sgnord[0:M,None,None]

        J1 = jv(ord[0:M+1,None],v1[None,:])[:,None,:]
        J2 = jv(ord[0:M+1,None],v2[None,:])[:,None,:]        
        dJ1 = self._derivJY(v1,J1[:,0,:])[:,None,:]
        dJ2 = self._derivJY(v2,J2[:,0,:])[:,None,:]
        j = n[:]//2

        dy[EV,:] = (self.p2n[j[EV],:]*sqrtq*np.sum(sgn*self.A[0:M,j[EV],0,:] *
                           (epz*J1[0:M,:,:]*dJ2[0:M,:,:] - enz*dJ1[0:M,:,:]*J2[0:M,:,:]),axis=0))

        dy[OD,:] = (-self.p2n1[j[OD],:]*sqrtq*np.sum(sgn*self.A[0:M,j[OD],1,:] *
                            (epz*J1[0:M,:,:]*dJ2[1:M+1,:,:] - enz*dJ1[0:M,:,:]*J2[1:M+1,:,:] +
                             epz*J1[1:M+1,:,:]*dJ2[0:M,:,:] - enz*dJ1[1:M+1,:,:]*J2[0:M,:,:]),axis=0))

        return self._cleanup(dy,zs)

    def dJo(self,n,z):
        """odd first-kind radial Mathieu function derivative (DJo) for q>0
        analogous to J Bessel functions, called Se in McLachlan, 
        for scalar or vector orders or argument

        n: int order (scalar or vector)
        z: float radial argument (scalar or vector)"""
        
        from scipy.special import jv
        n,z,sqrtq,enz,epz,v1,v2,M,EV,OD,zs = self._radDerivFuncSetup(n,z)
        dy = np.empty((n.shape[0],z.shape[0]),dtype=complex)

        ord = self._ord[0:M+2]
        sgn = self._sgnord[0:M,None,None]
        J1 = jv(ord[0:M+2,None],v1[None,:])[:,None,:]
        J2 = jv(ord[0:M+2,None],v2[None,:])[:,None,:]
        dJ1 = self._derivJY(v1,J1[0:M+2,0,:])[:,None,:]
        dJ2 = self._derivJY(v2,J2[0:M+2,0,:])[:,None,:]
        j = (n[:]-1)//2

        dy[EV,:] = (-self.s2n2[j[EV],:]*sqrtq*np.sum(sgn*self.B[0:M,j[EV],0,:] *
                            (epz*J1[0:M,:,:]*dJ2[2:M+2,:,:] - enz*dJ1[0:M,:,:]*J2[2:M+2,:,:] - 
                             (epz*J1[2:M+2,:,:]*dJ2[0:M,:,:] - enz*dJ1[2:M+2,:,:]*J2[0:M,:,:])),axis=0))

        dy[OD,:] = (self.s2n1[j[OD],:]*sqrtq*np.sum(sgn*self.B[0:M,j[OD],1,:] * 
                           (epz*J1[0:M,:,:]*dJ2[1:M+1,:,:] - enz*dJ1[0:M,:,:]*J2[1:M+1,:,:] - 
                            (epz*J1[1:M+1,:,:]*dJ2[0:M,:,:] - enz*dJ1[1:M+1,:,:]*J2[0:M,:,:])),axis=0))
        
        dy[n==0,:] = np.NaN # dJo_0() invalid
        return self.cleanup(dy,zs)

    def dNe(self,n,z):
        """even second-kind radial Mathieu function derivative (DNe) for q>0
        analogous to Y or N Bessel functions, called Fey in McLachlan, 
        for scalar or vector orders or argument

        n: int order (scalar or vector)
        z: float radial argument (scalar or vector)"""

        from scipy.special import yv,jv
        n,z,sqrtq,enz,epz,v1,v2,M,EV,OD,zs = self._radDerivFuncSetup(n,z)
        dy = np.empty((n.shape[0],z.shape[0]),dtype=complex)

        ord = self._ord[0:M+1]
        sgn = self._sgnord[0:M,None,None]
        J = jv(ord[0:M+1,None],v1[None,:])[:,None,:]
        N = yv(ord[0:M+1,None],v2[None,:])[:,None,:]
        dJ = self._derivJY(v1,J[0:M+1,0,:])[:,None,:]
        dN = self._derivJY(v2,N[0:M+1,0,:])[:,None,:]
        j = n[:]//2

        dy[EV,:] = (self.p2n[j[EV],:]*sqrtq*np.sum(sgn*self.A[0:M,j[EV],0,:] *
                           (epz*J[0:M,:,:]*dN[0:M,:,:] - enz*dJ[0:M,:,:]*N[0:M,:,:]),axis=0))

        dy[OD,:] = (-self.p2n1[j[OD],:]*sqrtq*np.sum(sgn*self.A[0:M,j[OD],1,:] *
                           (epz*J[0:M,:,:]*dN[1:M+1,:,:] - enz*dJ[0:M,:,:]*N[1:M+1,:,:] - 
                            (epz*J[1:M+1,:,:]*dN[0:M,:,:] - enz*dJ[1:M+1,:,:]*N[0:M,:,:])),axis=0))

        return self._cleanup(dy,zs)

    def dNo(self,n,z):
        """odd second-kind radial Mathieu function derivative (DNo) for q>0 
        analogous to Y or N Bessel functions, called Gey in McLachlan, 
        for scalar or vector orders or argument

        n: int order (scalar or vector)
        z: float radial argument (scalar or vector)"""
        
        from scipy.special import yv,jv
        n,z,sqrtq,enz,epz,v1,v2,M,EV,OD,zs = self._radDerivFuncSetup(n,z)
        dy = np.empty((n.shape[0],z.shape[0]),dtype=complex)

        ord = self._ord[0:M+2]
        sgn = self._sgnord[0:M,None,None]
        J = jv(ord[0:M+2,None],v1[None,:])[:,None,:]
        N = yv(ord[0:M+2,None],v2[None,:])[:,None,:]
        dJ = self._derivJY(v1,J[0:M+2,0,:])[:,None,:]
        dN = self._derivJY(v2,N[0:M+2,0,:])[:,None,:]
        j = (n[:]-1)//2

        dy[EV,:] = (-self.s2n2[j[EV],:]*sqrtq*np.sum(sgn*self.B[0:M,j[EV],0,:] *
                            (epz*J[0:M,:,:]*dN[2:M+2,:,:] - enz*dJ[0:M,:,:]*N[2:M+2,:,:] -
                             (epz*J[2:M+2,:,:]*dN[0:M,:,:] - enz*dJ[2:M+2,:,:]*N[0:M,:,:])),axis=0))

        dy[OD,:] = (self.s2n1[j[OD],:]*sqrtq*np.sum(sgn*self.B[0:M,j[OD],1,:] *
                            (epz*J[0:M,:,:]*dN[1:M+1,:,:] - enz*dJ[0:M,:,:]*N[1:M+1,:,:] + 
                             epz*J[1:M+1,:,:]*dN[0:M,:,:] - enz*dJ[1:M+1,:,:]*N[0:M,:,:]),axis=0))

        dy[n==0,:] = np.NaN  # dNo_0() invalid
        return self._cleanup(dy,zs)

