import numpy as np
import mathieu_functions as mf
import matplotlib.pyplot as plt

#$Id: mathieu_norm_check.py,v 1.1 2009/08/09 18:11:21 kris Exp kris $

plots = True
fcn_check = True
values = False

mv = []

if plots:
    # colors
    c = ['r','g','b','k','m','c','y']
    
    qr = np.linspace(0,40,100)
    
    a = np.zeros((qr.shape[0],6,12))
    b = np.zeros((qr.shape[0],6,12))
    
    for j,x in enumerate(qr):
        mv.append(mf.mathieu(q=x+1.0j, norm=2, M=50))
        a[j,0:6,0:3] = mv[-1].A[0:6,0:3,0,0] # A even
        a[j,0:6,3:6] = mv[-1].A[0:6,0:3,1,0] # A odd
        a[j,0:6,6:9] = mv[-1].B[0:6,0:3,0,0] # B even 
        a[j,0:6,9:12]= mv[-1].B[0:6,0:3,1,0] # B odd
        
    title = ['ce_{0}','ce_{2}','ce_{4}','se_{1}','se_{3}','se_{5}',
             'se_{2}','se_{4}','se_{6}','ce_{1}','ce_{3}','ce_{5}']
    
    fn = ['ce0','ce2','ce4','se1','se3','se5',
          'se2','se4','se6','ce1','ce3','ce5']
    
    ylab = ['A^{0}_{2n}','A^{2}_{2n}','A^{4}_{2n}',
            'A^{1}_{2n+1}','A^{3}_{2n+1}','A^{5}_{2n+1}',
            'B^{1}_{2n+2}','B^{3}_{2n+2}','B^{5}_{2n+2}',
            'B^{1}_{2n+1}','B^{3}_{2n+1}','B^{5}_{2n+1}']
    
    for j in range(12):
        plt.figure(1)
        for i in range(6):
            plt.plot(qr,a[:,i,j],c[i]+'-')
        plt.xlabel('$-q$')
        plt.ylabel('$'+ylab[j]+'$')
        plt.title('$'+title[j]+'$ coefficients')
        plt.savefig(fn[j]+'_coeff_fcn_q.png')
        plt.close(1)

    if fcn_check:
        num_ord = 4
        norm_check = np.zeros((len(mv),num_ord,4),dtype=complex)
        ord = np.arange(num_ord)
        for j,m in enumerate(mv):
            # Aev : check ce_2n(psi=0)
            norm_check[j,:,0] = m.ce(2*ord,0.0)
    
            # Aod : check se'_2n+1(psi=0)
            norm_check[j,:,1] = m.dse(2*ord+1,0.0)
    
            # Bev : check se'_2n+2(psi=0)
            norm_check[j,:,2] = m.dse(2*ord+2,0.0)
    
            # Bod : check ce_2n+1(psi=0)
            norm_check[j,:,3] = m.ce(2*ord+1,0.0)
        
        ylab = [r'$A_{ev} \quad ce_{2n}(\psi=0)$',
                r'$A_{od} \quad \partial / \partial \psi \left( se_{2n+1}(\psi=0)\right)$',
                r'$B_{ev} \quad \partial / \partial \psi \left( se_{2n+2}(\psi=0)\right)$',
                r'$B_{od} \quad ce_{2n+1}(\psi=0)$']

        plt.figure(2,figsize=(12,9))
        for k in [0,1,2,3]:
            for n in ord:
                plt.subplot(2,2,k+1)
                plt.plot(qr,norm_check[:,n,k].real,'-'+c[n])
                plt.plot(qr,norm_check[:,n,k].imag,':'+c[n])
            plt.xlabel('-q')
            plt.ylabel(ylab[k])
            plt.title('q=x + %g' % (m.q.imag,))
        
        plt.savefig('check_norm_fcn_values.png')
        plt.close(2)
if values:
    ########################################
    from scipy.integrate import trapz
    
    # check McLachlan normalization by "brute force" integration
    
    # McLachlan / Goldstein norm is defined by integral of
    # function squared.  All functions should be pi, except
    # ce_0(), which should be 2*pi

    # tables of the normalization for the norm=2 are in the 
    # back of Morse & Feshbach (pp1936-1937)

    hsq = np.linspace(0.0,9.0,num=10)
    qvec = hsq/4.0

    for norm in [1,2]:
    
        for q in qvec:

            if norm == 1:
                # McLachlan
                fc = open('norm_%i_%.3f_ce_int_check.txt'%(norm,q),'w')
                fs = open('norm_%i_%.3f_se_int_check.txt'%(norm,q),'w')
                factor = np.pi
            elif norm ==2:
                # Morse
                fc = open('norm_%i_%.0f_ce_int_check.txt'%(norm,q*4.0),'w')
                fs = open('norm_%i_%.0f_se_int_check.txt'%(norm,q*4.0),'w')
                factor = 1.0
                
            m = mf.mathieu(q=q+0.0j,norm=norm)
            maxn = 6
            
            n = np.arange(maxn)
            
            fc.write('#'+' '.join(['%19s'%x for x in ['dx']+range(maxn-1)])+'\n')
            fs.write('#'+' '.join(['%19s'%x for x in ['dx']+range(1,maxn)])+'\n')
            
            for dx in np.linspace(0.001,0.5,num=6):
                num = int(2.0*np.pi/dx)
                z = np.linspace(0,2*np.pi,num=num)
                dx2 = z[1]-z[0]
#                print dx,num,dx2
                ce = m.ce(n[:-1],z)
                fc.write('%.14e'%dx2+' '+
                         ' '.join(['%.14e'%x for x in list(trapz(ce**2,z,axis=1)/factor)])+'\n')
                se = m.se(n[1:],z)
                fs.write('%.14e'%dx2+' '+
                         ' '.join(['%.14e'%x for x in list(trapz(se**2,z,axis=1)/factor)])+'\n')
            
            fc.close()
            fs.close()

