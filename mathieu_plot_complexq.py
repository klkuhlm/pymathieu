import numpy as np
import mathieu_functions as mf
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

tiny = 1.0E-15

phi = np.linspace(0,np.pi/2.0,200)
eta = np.linspace(0,2.0,200)

def angular_plot_stuff(ylabel,title):
    plt.xlabel('$\\psi$')
    plt.xticks(np.linspace(0,np.pi/2.0,5),('$0$','$\\pi/8$','$\\pi/4$','$3\\pi/8$','$\\pi/2$'))
    plt.title(title)
    plt.ylabel(ylabel)
    plt.xlim([0,np.pi/2.0])

def radial_plot_stuff(ylabel,title):
    plt.xlabel('$\\sinh(\\eta)$')
    plt.ylabel(ylabel)
    plt.title(title)
    plt.xlim([0,np.sinh(np.max(eta))])

# ##################################################
# create plots of functions for ranges of argument and 
# different real-only value of q for the same order

c = ['k','r','b','g','m','c','y']

af = ['m.ce', 'm.se']
ad = ['m.dce', 'm.dse']

rf = ['m.Ie','m.Io','m.Ke','m.Ko']
rd = ['m.dIe','m.dIo','m.dKe','m.dKo']

# make a list of classes, one item for each q
mlist = []
qr = [0.01,0.1,1.0,10.0,100.0]

for val in qr:
    mlist.append(mf.mathieu(q=val+val*1j,norm=2,M=45))

# plot angular functions
for f,d in zip(af,ad):
    for n in [0,1,2,3]:
        if f[2:] == 'se' and n == 0: 
            continue

        plt.figure(figsize=(10,6))

        for j,m in enumerate(mlist):
            
            # plot functions on left, derivs on right
            # plot real part as solid, imaginary as dashed
            plt.subplot(121)
            plt.plot(phi,eval(f)(n,phi).real,c[j]+'-')
            plt.plot(phi,eval(f)(n,phi).imag,c[j]+'--')

            plt.subplot(122)
            plt.plot(phi,eval(d)(n,phi).real,c[j]+'-')        
            plt.plot(phi,eval(d)(n,phi).imag,c[j]+'--')
        
        plt.subplot(121)
        angular_plot_stuff('angular Mathieu fcn','$%s_%i(\\psi)$' % (f[2:],n))
        
        plt.subplot(122)    
        angular_plot_stuff('','$\\partial/\\partial \\psi$ MF')

        leg = plt.legend(['$%s_%i(\\psi,%.2f)$' % (f[2:],n,q) for q in qr],loc='best')
        for t in leg.get_texts():
            t.set_fontsize('small')
        plt.savefig('fcn_check_cmplx_q_range_%s_n%i.png' % (f[2:],n))

# plot radial functions
for f,d in zip(rf,rd):
    for n in [0,1,2,3]:
        if (f[2:] == 'Ko' or f[2:] == 'Io') and n == 0: 
            continue

        plt.figure(figsize=(10,6))

        for j,m in enumerate(mlist):
            
            # plot functions on left, derivs on right
            plt.subplot(121)
            z = eval(f)(n,eta)
            z[np.abs(z)< tiny] = 0.0
            plt.plot(np.sinh(eta),np.log10(np.abs(z.real)),c[j]+'-')
            plt.plot(np.sinh(eta),np.log10(np.abs(z.imag)),c[j]+'--')

            plt.subplot(122)
            z = eval(d)(n,eta)
            z[np.abs(z)< tiny] = 0.0
            plt.plot(np.sinh(eta),np.log10(np.abs(z.real)),c[j]+'-')        
            plt.plot(np.sinh(eta),np.log10(np.abs(z.imag)),c[j]+'--')
        
        plt.subplot(121)
        radial_plot_stuff('radial Mathieu fcn','$%s_%i(\\eta)$' %(f[2:],n))
        
        plt.subplot(122)    
        radial_plot_stuff('','$|\\partial / \\partial \\eta$ MF $|$')

        plt.savefig('fcn_check_cmplx_q_range_%s_n%i.png' % (f[2:],n))

        # same thing linear rather than log scale
        plt.figure(figsize=(10,6))
        for j,m in enumerate(mlist[0:3]):
            
            # plot real part of functions on left, real part of derivs on right
            plt.subplot(121)
            z = eval(f)(n,eta)
            z[np.abs(z)< tiny] = 0.0
            plt.plot(np.sinh(eta),z.real,c[j]+'-')
            plt.plot(np.sinh(eta),z.imag,c[j]+'--')

            plt.subplot(122)
            z = eval(d)(n,eta)
            z[np.abs(z)< tiny] = 0.0
            plt.plot(np.sinh(eta),z.real,c[j]+'-')        
            plt.plot(np.sinh(eta),z.imag,c[j]+'--')
        
        plt.subplot(121)
        radial_plot_stuff('radial Mathieu fcn','$%s_%i(\\eta)$' % (f[2:],n))
        
        plt.subplot(122)    
        radial_plot_stuff('','$|\\partial / \\partial \\eta$ MF $|$')

        leg = plt.legend(['$%s_%i(\\eta,%.2f)$' % (f[2:],n,q) for q in qr],loc='best')
        for t in leg.get_texts():
            t.set_fontsize('small')
        plt.savefig('fcn_check_cmplx_q_range_lin_%s_n%i.png' % (f[2:],n))

