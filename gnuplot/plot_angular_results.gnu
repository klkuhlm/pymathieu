q="1.0 + 1.0i"
# line styles used in all plots
set style line 1 linetype 1 linecolor rgb "red" linewidth 3
set style line 2 linetype 2 linecolor rgb "red" linewidth 0.5
set style line 3 linetype 1 linecolor rgb "orange" linewidth 3
set style line 4 linetype 2 linecolor rgb "orange" linewidth 0.5
set style line 5 linetype 1 linecolor rgb "black" linewidth 3
set style line 6 linetype 2 linecolor rgb "black" linewidth 0.5
set style line 7 linetype 1 linecolor rgb "green" linewidth 3 
set style line 8 linetype 2 linecolor rgb "green" linewidth 0.5
set style line 9 linetype 1 linecolor rgb "cyan" linewidth 3
set style line 10 linetype 2 linecolor rgb "cyan" linewidth 0.5
set style line 11 linetype 1 linecolor rgb "blue" linewidth 3
set style line 12 linetype 2 linecolor rgb "blue" linewidth 0.5
set style line 13 linetype 1 linecolor rgb "violet" linewidth 3
set style line 14 linetype 2 linecolor rgb "violet" linewidth 0.5
#
#
set title 'ce_n({/Symbol y};-q), q='.q
set key outside right bottom horizontal Right noreverse enhanced
set ylabel 'ce_n({/Symbol y};-q)'
set xlabel '{/Symbol y}'
unset logscale x
unset logscale y
unset xlabel
set xrange [0:pi]
set autoscale y
set xtics ('0' 0, '' pi/8,'{/Symbol p}/4' pi/4, '' 3*pi/8,'{/Symbol p}/2' pi/2, '' 5*pi/8, \
  '3{/Symbol p}/4' 3*pi/4, '' 7*pi/8, '{/Symbol p}' pi)
set terminal postscript eps enhanced color 'Times Roman' 20
set output 'ce_bench.eps'
plot 'ce_py_re.out' u 1:2 title '{/Symbol \302}[ce_0]' with line ls 1, \
     'ce_py_re.out' u 1:3 title '{/Symbol \301}[ce_0]' with line ls 2, \
     'ce_py_re.out' u 1:4 title '{/Symbol \302}[ce_1]' with line ls 3, \
     'ce_py_re.out' u 1:5 title '{/Symbol \301}[ce_1]' with line ls 4, \
     'ce_py_re.out' u 1:6 title '{/Symbol \302}[ce_2]' with line ls 5, \
     'ce_py_re.out' u 1:7 title '{/Symbol \301}[ce_2]' with line ls 6, \
     'ce_py_re.out' u 1:8 title '{/Symbol \302}[ce_3]' with line ls 7, \
     'ce_py_re.out' u 1:9 title '{/Symbol \301}[ce_3]' with line ls 8, \
     'ce_py_re.out' u 1:10 title '{/Symbol \302}[ce_4]' with line ls 9, \
     'ce_py_re.out' u 1:11 title '{/Symbol \301}[ce_4]' with line ls 10, \
     'ce_py_re.out' u 1:12 title '{/Symbol \302}[ce_5]' with line ls 11, \
     'ce_py_re.out' u 1:13 title '{/Symbol \301}[ce_5]' with line ls 12
##
##
##
set title 'se_n({/Symbol y};-q), q='.q
set ylabel 'se_n({/Symbol y};-q)'
set autoscale y
set terminal postscript eps enhanced color 'Times Roman' 20
set output 'se_bench.eps'
plot 'se_bench.out' u 1:4 title '{/Symbol \302}[se_1]' with line ls 1, \
     'se_bench.out' u 1:5 title '{/Symbol \301}[se_1]' with line ls 2, \
     'se_bench.out' u 1:6 title '{/Symbol \302}[se_2]' with line ls 3, \
     'se_bench.out' u 1:7 title '{/Symbol \301}[se_2]' with line ls 4, \
     'se_bench.out' u 1:8 title '{/Symbol \302}[se_3]' with line ls 5, \
     'se_bench.out' u 1:9 title '{/Symbol \301}[se_3]' with line ls 6, \
     'se_bench.out' u 1:10 title '{/Symbol \302}[se_4]' with line ls 7, \
     'se_bench.out' u 1:11 title '{/Symbol \301}[se_4]' with line ls 8, \
     'se_bench.out' u 1:12 title '{/Symbol \302}[se_5]' with line ls 9, \
     'se_bench.out' u 1:13 title '{/Symbol \301}[se_5]' with line ls 10, \
     'se_bench.out' u 1:14 title '{/Symbol \302}[se_6]' with line ls 11, \
     'se_bench.out' u 1:15 title '{/Symbol \301}[se_6]' with line ls 12
##
##
##
set title 'Dce_n({/Symbol y};-q), q='.q
set ylabel '{/Symbol \266} ce_n({/Symbol y};-q)/{/Symbol \266 y}'
set autoscale y
set terminal postscript eps enhanced color 'Times Roman' 20
set output 'Dce_bench.eps'
plot 'Dce_bench.out' u 1:2 title '{/Symbol \302}[Dce_0]' with line ls 1, \
     'Dce_bench.out' u 1:3 title '{/Symbol \301}[Dce_0]' with line ls 2, \
     'Dce_bench.out' u 1:4 title '{/Symbol \302}[Dce_1]' with line ls 3, \
     'Dce_bench.out' u 1:5 title '{/Symbol \301}[Dce_1]' with line ls 4, \
     'Dce_bench.out' u 1:6 title '{/Symbol \302}[Dce_2]' with line ls 5, \
     'Dce_bench.out' u 1:7 title '{/Symbol \301}[Dce_2]' with line ls 6, \
     'Dce_bench.out' u 1:8 title '{/Symbol \302}[Dce_3]' with line ls 7, \
     'Dce_bench.out' u 1:9 title '{/Symbol \301}[Dce_3]' with line ls 8, \
     'Dce_bench.out' u 1:10 title '{/Symbol \302}[Dce_4]' with line ls 9, \
     'Dce_bench.out' u 1:11 title '{/Symbol \301}[Dce_4]' with line ls 10, \
     'Dce_bench.out' u 1:12 title '{/Symbol \302}[Dce_5]' with line ls 11, \
     'Dce_bench.out' u 1:13 title '{/Symbol \301}[Dce_5]' with line ls 12
##
##
##
set title 'Dse_n({/Symbol y};-q), q='.q
set ylabel '{/Symbol \266} se_n({/Symbol y};-q)/{/Symbol \266 y}'
set autoscale y
set terminal postscript eps enhanced color 'Times Roman' 20
set output 'Dse_bench.eps'
plot 'Dse_bench.out' u 1:4 title '{/Symbol \302}[Dse_1]' with line ls 1, \
     'Dse_bench.out' u 1:5 title '{/Symbol \301}[Dse_1]' with line ls 2, \
     'Dse_bench.out' u 1:6 title '{/Symbol \302}[Dse_2]' with line ls 3, \
     'Dse_bench.out' u 1:7 title '{/Symbol \301}[Dse_2]' with line ls 4, \
     'Dse_bench.out' u 1:8 title '{/Symbol \302}[Dse_3]' with line ls 5, \
     'Dse_bench.out' u 1:9 title '{/Symbol \301}[Dse_3]' with line ls 6, \
     'Dse_bench.out' u 1:10 title '{/Symbol \302}[Dse_4]' with line ls 7, \
     'Dse_bench.out' u 1:11 title '{/Symbol \301}[Dse_4]' with line ls 8, \
     'Dse_bench.out' u 1:12 title '{/Symbol \302}[Dse_5]' with line ls 9, \
     'Dse_bench.out' u 1:13 title '{/Symbol \301}[Dse_5]' with line ls 10, \
     'Dse_bench.out' u 1:14 title '{/Symbol \302}[Dse_6]' with line ls 11, \
     'Dse_bench.out' u 1:15 title '{/Symbol \301}[Dse_6]' with line ls 12
