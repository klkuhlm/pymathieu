# set the value of q for all plots here
q = "1.0 + 1.0i"
# line styles used in all plots
set style line 1 linetype 1 linecolor rgb "red" linewidth 3
set style line 2 linetype 2 linecolor rgb "red" linewidth 0.5
set style line 3 linetype 1 linecolor rgb "orange" linewidth 3
set style line 4 linetype 2 linecolor rgb "orange" linewidth 0.5
set style line 5 linetype 1 linecolor rgb "black" linewidth 3
set style line 6 linetype 2 linecolor rgb "black" linewidth 0.5
set style line 7 linetype 1 linecolor rgb "green" linewidth 3 
set style line 8 linetype 2 linecolor rgb "green" linewidth 0.5
set style line 9 linetype 1 linecolor rgb "cyan" linewidth 3
set style line 10 linetype 2 linecolor rgb "cyan" linewidth 0.5
set style line 11 linetype 1 linecolor rgb "blue" linewidth 3
set style line 12 linetype 2 linecolor rgb "blue" linewidth 0.5
set style line 13 linetype 1 linecolor rgb "violet" linewidth 3
set style line 14 linetype 2 linecolor rgb "violet" linewidth 0.5
#
#
set title 'Ke_n({/Symbol h};-q), q='.q
set key outside right bottom horizontal Right noreverse enhanced
set ylabel 'Ke_n({/Symbol h};-q)/Ke_n(0;-q)'
set xlabel '{/Symbol h}'
set xtics autofreq
set xrange [1.0E-3:2.0]
set logscale x
unset logscale y
set autoscale y
set terminal postscript eps enhanced color 'Times Roman' 20
set output 'Ke_bench.eps'
plot 'Ke_norm_bench.out' u 1:2 title '{/Symbol \302}[Ke_0]' with line ls 1, \
     'Ke_norm_bench.out' u 1:3 title '{/Symbol \301}[Ke_0]' with line ls 2, \
     'Ke_norm_bench.out' u 1:4 title '{/Symbol \302}[Ke_1]' with line ls 3, \
     'Ke_norm_bench.out' u 1:5 title '{/Symbol \301}[Ke_1]' with line ls 4, \
     'Ke_norm_bench.out' u 1:6 title '{/Symbol \302}[Ke_2]' with line ls 5, \
     'Ke_norm_bench.out' u 1:7 title '{/Symbol \301}[Ke_2]' with line ls 6, \
     'Ke_norm_bench.out' u 1:8 title '{/Symbol \302}[Ke_3]' with line ls 7, \
     'Ke_norm_bench.out' u 1:9 title '{/Symbol \301}[Ke_3]' with line ls 8, \
     'Ke_norm_bench.out' u 1:10 title '{/Symbol \302}[Ke_4]' with line ls 9, \
     'Ke_norm_bench.out' u 1:11 title '{/Symbol \301}[Ke_4]' with line ls 10, \
     'Ke_norm_bench.out' u 1:12 title '{/Symbol \302}[Ke_5]' with line ls 11, \
     'Ke_norm_bench.out' u 1:13 title '{/Symbol \301}[Ke_5]' with line ls 12
##
##
##
set title 'Ko_n({/Symbol h};-q), q='.q
set ylabel 'Ko_n({/Symbol h};-q)/Ko_n({0;-q)'
set terminal postscript eps enhanced color 'Times Roman' 20
set output 'Ko_bench.eps'
plot 'Ko_norm_bench.out' u 1:4 title '{/Symbol \302}[Ko_1]' with line ls 1, \
     'Ko_norm_bench.out' u 1:5 title '{/Symbol \301}[Ko_1]' with line ls 2, \
     'Ko_norm_bench.out' u 1:6 title '{/Symbol \302}[Ko_2]' with line ls 3, \
     'Ko_norm_bench.out' u 1:7 title '{/Symbol \301}[Ko_2]' with line ls 4, \
     'Ko_norm_bench.out' u 1:8 title '{/Symbol \302}[Ko_3]' with line ls 5, \
     'Ko_norm_bench.out' u 1:9 title '{/Symbol \301}[Ko_3]' with line ls 6, \
     'Ko_norm_bench.out' u 1:10 title '{/Symbol \302}[Ko_4]' with line ls 7, \
     'Ko_norm_bench.out' u 1:11 title '{/Symbol \301}[Ko_4]' with line ls 8, \
     'Ko_norm_bench.out' u 1:12 title '{/Symbol \302}[Ko_5]' with line ls 9, \
     'Ko_norm_bench.out' u 1:13 title '{/Symbol \301}[Ko_5]' with line ls 10, \
     'Ko_norm_bench.out' u 1:14 title '{/Symbol \302}[Ko_6]' with line ls 11, \
     'Ko_norm_bench.out' u 1:15 title '{/Symbol \301}[Ko_6]' with line ls 12
##
##
##
set title 'DKe_n({/Symbol h};-q), q='.q
set ylabel 'normalized {/Symbol \266} Ke_n({/Symbol h};-q)/{/Symbol \266 h}'
set terminal postscript eps enhanced color 'Times Roman' 20
set output 'DKe_bench.eps'
plot 'DKe_norm_bench.out' u 1:2 title '{/Symbol \302}[DKe_0]' with line ls 1, \
     'DKe_norm_bench.out' u 1:3 title '{/Symbol \301}[DKe_0]' with line ls 2, \
     'DKe_norm_bench.out' u 1:4 title '{/Symbol \302}[DKe_1]' with line ls 3, \
     'DKe_norm_bench.out' u 1:5 title '{/Symbol \301}[DKe_1]' with line ls 4, \
     'DKe_norm_bench.out' u 1:6 title '{/Symbol \302}[DKe_2]' with line ls 5, \
     'DKe_norm_bench.out' u 1:7 title '{/Symbol \301}[DKe_2]' with line ls 6, \
     'DKe_norm_bench.out' u 1:8 title '{/Symbol \302}[DKe_3]' with line ls 7, \
     'DKe_norm_bench.out' u 1:9 title '{/Symbol \301}[DKe_3]' with line ls 8, \
     'DKe_norm_bench.out' u 1:10 title '{/Symbol \302}[DKe_4]' with line ls 9, \
     'DKe_norm_bench.out' u 1:11 title '{/Symbol \301}[DKe_4]' with line ls 10, \
     'DKe_norm_bench.out' u 1:12 title '{/Symbol \302}[DKe_5]' with line ls 11, \
     'DKe_norm_bench.out' u 1:13 title '{/Symbol \301}[DKe_5]' with line ls 12
##
##
##
set title 'DKo_n({/Symbol h};-q), q='.q
set ylabel 'normalized {/Symbol \266} Ko_n({/Symbol h};-q)/{/Symbol \266 h}'
set terminal postscript eps enhanced color 'Times Roman' 20
set output 'DKo_bench.eps'
plot 'DKo_norm_bench.out' u 1:4 title '{/Symbol \302}[DKo_1]' with line ls 1, \
     'DKo_norm_bench.out' u 1:5 title '{/Symbol \301}[DKo_1]' with line ls 2, \
     'DKo_norm_bench.out' u 1:6 title '{/Symbol \302}[DKo_2]' with line ls 3, \
     'DKo_norm_bench.out' u 1:7 title '{/Symbol \301}[DKo_2]' with line ls 4, \
     'DKo_norm_bench.out' u 1:8 title '{/Symbol \302}[DKo_3]' with line ls 5, \
     'DKo_norm_bench.out' u 1:9 title '{/Symbol \301}[DKo_3]' with line ls 6, \
     'DKo_norm_bench.out' u 1:10 title '{/Symbol \302}[DKo_4]' with line ls 7, \
     'DKo_norm_bench.out' u 1:11 title '{/Symbol \301}[DKo_4]' with line ls 8, \
     'DKo_norm_bench.out' u 1:12 title '{/Symbol \302}[DKo_5]' with line ls 9, \
     'DKo_norm_bench.out' u 1:13 title '{/Symbol \301}[DKo_5]' with line ls 10, \
     'DKo_norm_bench.out' u 1:14 title '{/Symbol \302}[DKo_6]' with line ls 11, \
     'DKo_norm_bench.out' u 1:15 title '{/Symbol \301}[DKo_6]' with line ls 12
#
#
set title 'Ie_n({/Symbol h};-q), q='.q
set key outside right bottom horizontal Right noreverse enhanced
set ylabel 'Ie_n({/Symbol h};-q)'
set xrange [0:1.0]
unset logscale x
set autoscale y
set logscale y
#set yrange [-50:450]
set terminal postscript eps enhanced color 'Times Roman' 20
set output 'Ie_bench.eps'
plot 'Ie_bench.out' u 1:2 title '{/Symbol \302}[Ie_0]' with line ls 1, \
     'Ie_bench.out' u 1:3 title '{/Symbol \301}[Ie_0]' with line ls 2, \
     'Ie_bench.out' u 1:4 title '{/Symbol \302}[Ie_1]' with line ls 3, \
     'Ie_bench.out' u 1:5 title '{/Symbol \301}[Ie_1]' with line ls 4, \
     'Ie_bench.out' u 1:6 title '{/Symbol \302}[Ie_2]' with line ls 5, \
     'Ie_bench.out' u 1:7 title '{/Symbol \301}[Ie_2]' with line ls 6, \
     'Ie_bench.out' u 1:8 title '{/Symbol \302}[Ie_3]' with line ls 7, \
     'Ie_bench.out' u 1:9 title '{/Symbol \301}[Ie_3]' with line ls 8, \
     'Ie_bench.out' u 1:10 title '{/Symbol \302}[Ie_4]' with line ls 9, \
     'Ie_bench.out' u 1:11 title '{/Symbol \301}[Ie_4]' with line ls 10, \
     'Ie_bench.out' u 1:12 title '{/Symbol \302}[Ie_5]' with line ls 11, \
     'Ie_bench.out' u 1:13 title '{/Symbol \301}[Ie_5]' with line ls 12
##
##
##
set title 'Io_n({/Symbol h};-q), q='.q
set ylabel 'Io_n({/Symbol h};-q)'
#unset autoscale y
set terminal postscript eps enhanced color 'Times Roman' 20
set output 'Io_bench.eps'
plot 'Io_bench.out' u 1:4 title '{/Symbol \302}[Io_1]' with line ls 1, \
     'Io_bench.out' u 1:5 title '{/Symbol \301}[Io_1]' with line ls 2, \
     'Io_bench.out' u 1:6 title '{/Symbol \302}[Io_2]' with line ls 3, \
     'Io_bench.out' u 1:7 title '{/Symbol \301}[Io_2]' with line ls 4, \
     'Io_bench.out' u 1:8 title '{/Symbol \302}[Io_3]' with line ls 5, \
     'Io_bench.out' u 1:9 title '{/Symbol \301}[Io_3]' with line ls 6, \
     'Io_bench.out' u 1:10 title '{/Symbol \302}[Io_4]' with line ls 7, \
     'Io_bench.out' u 1:11 title '{/Symbol \301}[Io_4]' with line ls 8, \
     'Io_bench.out' u 1:12 title '{/Symbol \302}[Io_5]' with line ls 9, \
     'Io_bench.out' u 1:13 title '{/Symbol \301}[Io_5]' with line ls 10, \
     'Io_bench.out' u 1:14 title '{/Symbol \302}[Io_6]' with line ls 11, \
     'Io_bench.out' u 1:15 title '{/Symbol \301}[Io_6]' with line ls 12
##
##
##
set title 'DIe_n({/Symbol h};-q), q='.q
set ylabel '{/Symbol \266} Ie_n({/Symbol h};-q)/{/Symbol \266 h}'
unset xlabel
#unset autoscale y
set terminal postscript eps enhanced color 'Times Roman' 20
set output 'DIe_bench.eps'
plot 'DIe_bench.out' u 1:2 title '{/Symbol \302}[DIe_0]' with line ls 1, \
     'DIe_bench.out' u 1:3 title '{/Symbol \301}[DIe_0]' with line ls 2, \
     'DIe_bench.out' u 1:4 title '{/Symbol \302}[DIe_1]' with line ls 3, \
     'DIe_bench.out' u 1:5 title '{/Symbol \301}[DIe_1]' with line ls 4, \
     'DIe_bench.out' u 1:6 title '{/Symbol \302}[DIe_2]' with line ls 5, \
     'DIe_bench.out' u 1:7 title '{/Symbol \301}[DIe_2]' with line ls 6, \
     'DIe_bench.out' u 1:8 title '{/Symbol \302}[DIe_3]' with line ls 7, \
     'DIe_bench.out' u 1:9 title '{/Symbol \301}[DIe_3]' with line ls 8, \
     'DIe_bench.out' u 1:10 title '{/Symbol \302}[DIe_4]' with line ls 9, \
     'DIe_bench.out' u 1:11 title '{/Symbol \301}[DIe_4]' with line ls 10, \
     'DIe_bench.out' u 1:12 title '{/Symbol \302}[DIe_5]' with line ls 11, \
     'DIe_bench.out' u 1:13 title '{/Symbol \301}[DIe_5]' with line ls 12
##
##
##
set title 'DIo_n({/Symbol h};-q), q='.q
set ylabel '{/Symbol \266} Io_n({/Symbol h};-q)/{/Symbol \266 h}'
unset xlabel
#unset autoscale y
set terminal postscript eps enhanced color 'Times Roman' 20
set output 'DIo_bench.eps'
plot 'DIo_bench.out' u 1:4 title '{/Symbol \302}[DIo_1]' with line ls 1, \
     'DIo_bench.out' u 1:5 title '{/Symbol \301}[DIo_1]' with line ls 2, \
     'DIo_bench.out' u 1:6 title '{/Symbol \302}[DIo_2]' with line ls 3, \
     'DIo_bench.out' u 1:7 title '{/Symbol \301}[DIo_2]' with line ls 4, \
     'DIo_bench.out' u 1:8 title '{/Symbol \302}[DIo_3]' with line ls 5, \
     'DIo_bench.out' u 1:9 title '{/Symbol \301}[DIo_3]' with line ls 6, \
     'DIo_bench.out' u 1:10 title '{/Symbol \302}[DIo_4]' with line ls 7, \
     'DIo_bench.out' u 1:11 title '{/Symbol \301}[DIo_4]' with line ls 8, \
     'DIo_bench.out' u 1:12 title '{/Symbol \302}[DIo_5]' with line ls 9, \
     'DIo_bench.out' u 1:13 title '{/Symbol \301}[DIo_5]' with line ls 10, \
     'DIo_bench.out' u 1:14 title '{/Symbol \302}[DIo_6]' with line ls 11, \
     'DIo_bench.out' u 1:15 title '{/Symbol \301}[DIo_6]' with line ls 12
##
##
##
set title 'Ke_{2n}({/Symbol h};-q)/DKe_{2n}(0;-q), q='.q
set ylabel 'flux function'
set logscale x
unset logscale y
set xrange [1.0E-3:2.0]
set autoscale y
set terminal postscript eps enhanced color 'Times Roman' 20
set output 'line_flux_bench.eps'
plot 'flux_source_bench.out' u 1:4 title '{/Symbol \302}[flux_0]' with line ls 1, \
     'flux_source_bench.out' u 1:5 title '{/Symbol \301}[flux_0]' with line ls 2, \
     'flux_source_bench.out' u 1:6 title '{/Symbol \302}[flux_2]' with line ls 3, \
     'flux_source_bench.out' u 1:7 title '{/Symbol \301}[flux_2]' with line ls 4, \
     'flux_source_bench.out' u 1:8 title '{/Symbol \302}[flux_4]' with line ls 5, \
     'flux_source_bench.out' u 1:9 title '{/Symbol \301}[flux_4]' with line ls 6, \
     'flux_source_bench.out' u 1:10 title '{/Symbol \302}[flux_6]' with line ls 7, \
     'flux_source_bench.out' u 1:11 title '{/Symbol \301}[flux_6]' with line ls 8, \
     'flux_source_bench.out' u 1:12 title '{/Symbol \302}[flux_8]' with line ls 9, \
     'flux_source_bench.out' u 1:13 title '{/Symbol \301}[flux_8]' with line ls 10
     
     
