# set the value of q for all plots here
#q = "5.0 + 0.0i"
# line styles used in all plots
set style line 1 linetype 1 linecolor rgb "red" linewidth 3
set style line 2 linetype 2 linecolor rgb "red" linewidth 0.5
set style line 3 linetype 1 linecolor rgb "orange" linewidth 3
set style line 4 linetype 2 linecolor rgb "orange" linewidth 0.5
set style line 5 linetype 1 linecolor rgb "black" linewidth 3
set style line 6 linetype 2 linecolor rgb "black" linewidth 0.5
set style line 7 linetype 1 linecolor rgb "green" linewidth 3 
set style line 8 linetype 2 linecolor rgb "green" linewidth 0.5
set style line 9 linetype 1 linecolor rgb "cyan" linewidth 3
set style line 10 linetype 2 linecolor rgb "cyan" linewidth 0.5
set style line 11 linetype 1 linecolor rgb "blue" linewidth 3
set style line 12 linetype 2 linecolor rgb "blue" linewidth 0.5
set style line 13 linetype 1 linecolor rgb "violet" linewidth 3
set style line 14 linetype 2 linecolor rgb "violet" linewidth 0.5
#
#
set title 'Fig 3-6a: Ie_0, -5<q<1'
set key outside right bottom horizontal Right noreverse enhanced
set ylabel 'Ie_0'
set xlabel '{/Symbol h}'
set xtics autofreq
set xrange [0:0.7]
unset logscale x
unset logscale y
#set yrange [0:4]
set autoscale y
set terminal postscript eps enhanced color 'Times Roman' 20
set output 'gv_fig36a_Ie0_qrange.eps'
plot 'Ie_bench_5.0.out' u 1:2 title 'Ie_0({/Symbol h};-5)' with line ls 1, \
     'Ie_bench_4.0.out' u 1:2 title 'Ie_0({/Symbol h};-4)' with line ls 3, \
     'Ie_bench_3.0.out' u 1:2 title 'Ie_0({/Symbol h};-3)' with line ls 5, \
     'Ie_bench_2.0.out' u 1:2 title 'Ie_0({/Symbol h};-2)' with line ls 7, \
     'Ie_bench_1.0.out' u 1:2 title 'Ie_0({/Symbol h};-1)' with line ls 9
#
#
set title 'Fig 3-6b: Io_1, -5<q<1'
set ylabel 'Io_1'
#set yrange [0:7]
set autoscale y
set output 'gv_fig36b_Io1_qrange.eps'
plot 'Io_bench_5.0.out' u 1:4 title 'Io_1({/Symbol h};-5)' with line ls 1, \
     'Io_bench_4.0.out' u 1:4 title 'Io_1({/Symbol h};-4)' with line ls 3, \
     'Io_bench_3.0.out' u 1:4 title 'Io_1({/Symbol h};-3)' with line ls 5, \
     'Io_bench_2.0.out' u 1:4 title 'Io_1({/Symbol h};-2)' with line ls 7, \
     'Io_bench_1.0.out' u 1:4 title 'Io_1({/Symbol h};-1)' with line ls 9
#
#
set title 'Fig 3-6c: Ke_3, -5<q<1'
set xrange [0:1]
set ylabel 'Ke_3'
set yrange [0:60]
set output 'gv_fig36c_Ke3_qrange.eps'
plot 'Ke_bench_3.5.out' u 1:8 title 'Ke_3({/Symbol h};-3.5)' with line ls 11, \
     'Ke_bench_3.0.out' u 1:8 title 'Ke_3({/Symbol h};-3)' with line ls 1, \
     'Ke_bench_2.5.out' u 1:8 title 'Ke_3({/Symbol h};-2.5)' with line ls 3, \
     'Ke_bench_2.0.out' u 1:8 title 'Ke_3({/Symbol h};-2)' with line ls 5, \
     'Ke_bench_1.5.out' u 1:8 title 'Ke_3({/Symbol h};-1.5)' with line ls 7, \
     'Ke_bench_1.0.out' u 1:8 title 'Ke_3({/Symbol h};-1)' with line ls 9
#
#
set title 'Fig 3-6d: Ko_1, -5<q<1'
set xrange [0:1]
set ylabel 'Ko_1'
#set yrange [0:0.35]
set autoscale y
set output 'gv_fig36d_Ko1_qrange.eps'
plot 'Ko_bench_3.5.out' u 1:4 title 'Ko_1({/Symbol h};-3.5)' with line ls 11, \
     'Ko_bench_3.0.out' u 1:4 title 'Ko_1({/Symbol h};-3)' with line ls 1, \
     'Ko_bench_2.5.out' u 1:4 title 'Ko_1({/Symbol h};-2.5)' with line ls 3, \
     'Ko_bench_2.0.out' u 1:4 title 'Ko_1({/Symbol h};-2)' with line ls 5, \
     'Ko_bench_1.5.out' u 1:4 title 'Ko_1({/Symbol h};-1.5)' with line ls 7, \
     'Ko_bench_1.0.out' u 1:4 title 'Ko_1({/Symbol h};-1)' with line ls 9
