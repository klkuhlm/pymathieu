set title 'even angular modified Mathieu functions ce_n({/Symbol y};q)'
set key outside below
set ylabel 'ce_n({/Symbol y};q)'
unset xlabel
set xrange [0:2*pi]
set autoscale y
set xtics ('0' 0, '' pi/4, '{/Symbol p}/2' pi/2, '' 3*pi/4, '{/Symbol p}' pi, \
  '' 5*pi/4, '3{/Symbol p}/2' 3*pi/2, '' 7*pi/4, '2{/Symbol p}' 2*pi)
set terminal postscript eps enhanced color 'Times Roman' 20
set output 'ce_bench.eps'
plot 'ce_bench.out' u 1:2 title 'Re[ce_0]' with line lt 1 lw 3, \
     'ce_bench.out' u 1:3 title 'Im[ce_0]' with line lt 1 lw 0.5, \
     'ce_bench.out' u 1:4 title 'Re[ce_1]' with line lt 2 lw 3, \
     'ce_bench.out' u 1:5 title 'Im[ce_1]' with line lt 2 lw 0.5, \
     'ce_bench.out' u 1:6 title 'Re[ce_2]' with line lt 3 lw 3, \
     'ce_bench.out' u 1:7 title 'Im[ce_2]' with line lt 3 lw 0.5, \
     'ce_bench.out' u 1:8 title 'Re[ce_3]' with line lt 4 lw 3, \
     'ce_bench.out' u 1:9 title 'Im[ce_3]' with line lt 4 lw 0.5, \
     'ce_bench.out' u 1:10 title 'Re[ce_4]' with line lt 5 lw 3, \
     'ce_bench.out' u 1:11 title 'Im[ce_4]' with line lt 5 lw 0.5, \
     'ce_bench.out' u 1:12 title 'Re[ce_5]' with line lt 7 lw 3, \
     'ce_bench.out' u 1:13 title 'Im[ce_5]' with line lt 7 lw 0.5
##
##
##
set title 'odd angular modified Mathieu functions se_n({/Symbol y};q)'
set key outside below
set ylabel 'se_n({/Symbol y};q)'
unset xlabel
set xrange [0:2*pi]
set autoscale y
set xtics ('0' 0, '' pi/4, '{/Symbol p}/2' pi/2, '' 3*pi/4, '{/Symbol p}' pi, \
  '' 5*pi/4, '3{/Symbol p}/2' 3*pi/2, '' 7*pi/4, '2{/Symbol p}' 2*pi)
set terminal postscript eps enhanced color 'Times Roman' 20
set output 'se_bench.eps'
plot 'se_bench.out' u 1:4 title 'Re[se_1]' with line lt 2 lw 3, \
     'se_bench.out' u 1:5 title 'Im[se_1]' with line lt 2 lw 0.5, \
     'se_bench.out' u 1:6 title 'Re[se_2]' with line lt 3 lw 3, \
     'se_bench.out' u 1:7 title 'Im[se_2]' with line lt 3 lw 0.5, \
     'se_bench.out' u 1:8 title 'Re[se_3]' with line lt 4 lw 3, \
     'se_bench.out' u 1:9 title 'Im[se_3]' with line lt 4 lw 0.5, \
     'se_bench.out' u 1:10 title 'Re[se_4]' with line lt 5 lw 3, \
     'se_bench.out' u 1:11 title 'Im[se_4]' with line lt 5 lw 0.5, \
     'se_bench.out' u 1:12 title 'Re[se_5]' with line lt 7 lw 3, \
     'se_bench.out' u 1:13 title 'Im[se_5]' with line lt 7 lw 0.5, \
     'se_bench.out' u 1:14 title 'Re[se_6]' with line lt 8 lw 3, \
     'se_bench.out' u 1:15 title 'Im[se_6]' with line lt 8 lw 0.5
##
##
##
set title 'derivative of even angular modified Mathieu functions ce_n({/Symbol y};q)'
set key outside below
set ylabel 'd ce_n({/Symbol y};q)/d{/Symbol y}'
unset xlabel
set xrange [0:2*pi]
set autoscale y
set xtics ('0' 0, '' pi/4, '{/Symbol p}/2' pi/2, '' 3*pi/4, '{/Symbol p}' pi, \
  '' 5*pi/4, '3{/Symbol p}/2' 3*pi/2, '' 7*pi/4, '2{/Symbol p}' 2*pi)
set terminal postscript eps enhanced color 'Times Roman' 20
set output 'Dce_bench.eps'
plot 'Dce_bench.out' u 1:2 title 'Re[Dce_0]' with line lt 1 lw 3, \
     'Dce_bench.out' u 1:3 title 'Im[Dce_0]' with line lt 1 lw 0.5, \
     'Dce_bench.out' u 1:4 title 'Re[Dce_1]' with line lt 2 lw 3, \
     'Dce_bench.out' u 1:5 title 'Im[Dce_1]' with line lt 2 lw 0.5, \
     'Dce_bench.out' u 1:6 title 'Re[Dce_2]' with line lt 3 lw 3, \
     'Dce_bench.out' u 1:7 title 'Im[Dce_2]' with line lt 3 lw 0.5, \
     'Dce_bench.out' u 1:8 title 'Re[Dce_3]' with line lt 4 lw 3, \
     'Dce_bench.out' u 1:9 title 'Im[Dce_3]' with line lt 4 lw 0.5, \
     'Dce_bench.out' u 1:10 title 'Re[Dce_4]' with line lt 5 lw 3, \
     'Dce_bench.out' u 1:11 title 'Im[Dce_4]' with line lt 5 lw 0.5, \
     'Dce_bench.out' u 1:12 title 'Re[Dce_5]' with line lt 7 lw 3, \
     'Dce_bench.out' u 1:13 title 'Im[Dce_5]' with line lt 7 lw 0.5
##
##
##
set title 'derivative of odd angular modified Mathieu functions se_n({/Symbol y};q)'
set key outside below
set ylabel 'd se_n({/Symbol y};q)/d{/Symbol y}'
unset xlabel
set xrange [0:2*pi]
set autoscale y
set xtics ('0' 0, '' pi/4, '{/Symbol p}/2' pi/2, '' 3*pi/4, '{/Symbol p}' pi, \
  '' 5*pi/4, '3{/Symbol p}/2' 3*pi/2, '' 7*pi/4, '2{/Symbol p}' 2*pi)
set terminal postscript eps enhanced color 'Times Roman' 20
set output 'Dse_bench.eps'
plot 'Dse_bench.out' u 1:4 title 'Re[Dse_1]' with line lt 2 lw 3, \
     'Dse_bench.out' u 1:5 title 'Im[Dse_1]' with line lt 2 lw 0.5, \
     'Dse_bench.out' u 1:6 title 'Re[Dse_2]' with line lt 3 lw 3, \
     'Dse_bench.out' u 1:7 title 'Im[Dse_2]' with line lt 3 lw 0.5, \
     'Dse_bench.out' u 1:8 title 'Re[Dse_3]' with line lt 4 lw 3, \
     'Dse_bench.out' u 1:9 title 'Im[Dse_3]' with line lt 4 lw 0.5, \
     'Dse_bench.out' u 1:10 title 'Re[Dse_4]' with line lt 5 lw 3, \
     'Dse_bench.out' u 1:11 title 'Im[Dse_4]' with line lt 5 lw 0.5, \
     'Dse_bench.out' u 1:12 title 'Re[Dse_5]' with line lt 7 lw 3, \
     'Dse_bench.out' u 1:13 title 'Im[Dse_5]' with line lt 7 lw 0.5, \
     'Dse_bench.out' u 1:14 title 'Re[Dse_6]' with line lt 8 lw 3, \
     'Dse_bench.out' u 1:15 title 'Im[Dse_6]' with line lt 8 lw 0.5
     
