import numpy as np
import mathieu_functions as mf
import matplotlib.pyplot as plt
from mathieu_deriv_check import fd_deriv as deriv

def ang_ode(f,d,m,n,z,q):
    """ angular ODE: modified Mathieu equation
    McLachlan, section 2.18 page 21
    y" + (a + 2q cos(2z))*y = 0 """
    return deriv(d,n,z,dx=1.0E-6) + (m + 2*q*np.cos(2.0*z))*f(n,z)

def rad_ode(f,d,m,n,z,q):
    """ radial ODE: modified Mathieu equation
    McLachlan, section 13.30 page 248
    y" - (a + 2q cosh(2z))*y = 0 """
    return deriv(d,n,z,dx=1.0E-8) - (m + 2*q*np.cosh(2.0*z))*f(n,z)

re = np.linspace(0.0,10.0,num=11)
#im = np.linspace(0.0,10.0,num=20)
#X,Y = np.meshgrid(re,im)
#Z = X + Y*1j

psi = np.linspace(0,2*np.pi)
eta = np.linspace(0,2.5)

n = np.arange(6)

arhs = np.empty((n.shape[0],psi.shape[0]),dtype=complex)
rrhs = np.empty((n.shape[0],eta.shape[0]),dtype=complex)

ang = False
rad = True

for norm in [1,2]:
    for i,q in enumerate(re):
        m = mf.mathieu(q=q,norm=norm)
        
        # for mathieu characteristic numbers (see fig 8B mclachlan)
        # mcn[:,j], j=0 a_{2n}     ce_2n
        #           j=1 a_{2n+1}   ce_2n+1
        #           j=2 b_{2n+2}   se_2n+2
        #           j=3 b_{2n+1}   se_2n+1

        mcn_ce = np.empty((2*m.mcn.shape[0],),dtype=complex)
        mcn_se = np.empty((2*m.mcn.shape[0],),dtype=complex)
            
        for k in range(m.mcn.shape[0]):
            mcn_ce[2*k]   = m.mcn[k,0]  # a_2n
            mcn_ce[2*k+1] = m.mcn[k,3]  # b_2n+1
    
            mcn_se[2*k]   = m.mcn[k,1]  # a_2n+1  
            mcn_se[2*k+1] = m.mcn[k,2]  # b_2n+2

        if ang:

            ########################################
            ## Angular modified Mathieu functions
            ########################################
    
            for j,nv in enumerate(n):            
                arhs[j,:] = ang_ode(m.ce,m.dce, mcn_ce[j], nv,psi,m.q)
    
            plt.figure()
            plt.subplot(121)
            for ord in n:
                plt.plot(psi,np.abs(arhs[ord,:]),label='n:%i a:%.1f'%(ord,mcn_ce[ord]))
            plt.title('$y^{\\prime \\prime} + (a + 2q \\cos(2 \\psi))y$; ce')
            plt.xlabel('$\\psi$')
            plt.ylabel('ce')
            plt.axis('tight')
            plt.legend(loc='best')
    
            for j,nv in enumerate(n[1:]):
                arhs[j,:] = ang_ode(m.se,m.dse, mcn_se[j], nv,psi,m.q)
    
            plt.subplot(122)
            for ord in n[1:]:
                plt.plot(psi,np.abs(arhs[ord,:]),label='n:%i a:%.1f'%(ord,mcn_se[ord-1]))
            plt.title('se; $q=$'+ str(q)+'+0j; norm='+str(norm))
            plt.xlabel('$\\psi$')
            plt.axis('tight')
            plt.legend(loc='best')
            plt.savefig('ode_angular_n'+str(norm)+'_q'+str(i)+'.png')
        
        if rad:

            ########################################
            ## Radial modified Mathieu functions
            ########################################
            
            for j,nv in enumerate(n):            
                rrhs[j,:] = rad_ode(m.Ie,m.dIe, mcn_ce[j], nv,eta,m.q)
    
            plt.figure()
            plt.subplot(221)
            for ord in n:
                plt.plot(np.sinh(eta),np.log10(np.abs(rrhs[ord,:])),
                         label='n:%i a:%.1f'%(ord,mcn_ce[ord]))
            plt.title('$y^{\\prime \\prime} - (a + 2q \\cosh(2 \\eta))y$; Ie')
            plt.ylabel('$\\log_{10}(Ie)$')
            plt.axis('tight')
            plt.legend(loc='best')
    
            for j,nv in enumerate(n[1:]):
                rrhs[j,:] = rad_ode(m.Io,m.dIo, mcn_se[j], nv,eta,m.q)
    
            plt.subplot(222)
            for ord in n[1:]:
                plt.plot(np.sinh(eta),np.log10(np.abs(rrhs[ord,:])),
                         label='n:%i a:%.1f'%(ord,mcn_se[ord-1]))
            plt.title('Io; $q=$'+ str(q)+'+0j; norm='+str(norm))
            plt.ylabel('$\\log_{10}(Io)$')
            plt.axis('tight')
            plt.legend(loc='best')
    
            for j,nv in enumerate(n):            
                rrhs[j,:] = rad_ode(m.Ke,m.dKe, mcn_ce[j], nv,eta,m.q)
    
            plt.subplot(223)
            for ord in n:
                plt.plot(np.sinh(eta),np.log10(np.abs(rrhs[ord,:])),
                         label='n:%i a:%.1f'%(ord,mcn_ce[ord]))
            plt.xlabel('$\\sinh(\\eta)$')
            plt.ylabel('$\\log_{10}(Ke)$')
            plt.axis('tight')
            plt.legend(loc='best')
    
            for j,nv in enumerate(n[1:]):
                rrhs[j,:] = rad_ode(m.Ko,m.dKo, mcn_se[j], nv,eta,m.q)
    
            plt.subplot(224)
            for ord in n[1:]:
                plt.plot(np.sinh(eta),np.log10(np.abs(rrhs[ord,:])),
                         label='n:%i a:%.1f'%(ord,mcn_se[ord-1]))
            plt.xlabel('$\\sinh(\\eta)$')
            plt.ylabel('$\\log_{10}(Ko)$')
            plt.axis('tight')
            plt.legend(loc='best')
    
            plt.savefig('ode_radial_n'+str(norm)+'_q'+str(i)+'.png')
            
